/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package controllers;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import controllers.updateMenus.UpdateRouteMenuController;
import controllers.updateMenus.UpdateStopMenuController;
import controllers.updateMenus.UpdateStopTimeMenuController;
import controllers.updateMenus.UpdateTripMenuController;
import dataManagement.Route;
import dataManagement.Stop;
import dataManagement.StopTime;
import dataManagement.Trip;
import fileManagement.FileHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import observers.Bus;
import observers.GTFSGoogleMapObserver;
import observers.viewers.RoutesViewer;
import observers.viewers.StopTimesViewer;
import observers.viewers.StopsViewer;
import observers.viewers.TripsViewer;
import subject.GTFSData;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Controller class for the 'main_menu.fxml' file.
 *
 * @author Noah Villanueva
 * @version 1.0
 * @created 10-Oct-2019 9:10:28 AM
 */
public class MainMenuController implements MapComponentInitializedListener {
    // Private variables
    private FileHandler fileHandler;
    private GTFSData gtfsData;
    private ImportMenuController importMenuController;
    private UpdateRouteMenuController updateRouteMenuController;
    private UpdateStopMenuController updateStopMenuController;
    private UpdateStopTimeMenuController updateStopTimeMenuController;
    private UpdateTripMenuController updateTripMenuController;

    // Observers
    private RoutesViewer routesViewer;
    private TripsViewer tripsViewer;
    private StopsViewer stopsViewer;
    private StopTimesViewer stopTimesViewer;
    private GTFSGoogleMapObserver gtfsGoogleMapObserver;

    // FXML variables
    @FXML
    MenuButton stopsSearchTarget;
    @FXML
    MenuButton tripsSearchTarget;
    @FXML
    RadioMenuItem stopNextTripRadioButton;
    @FXML
    RadioMenuItem stopRoutesContainingStopButton;
    @FXML
    RadioMenuItem stopTripsContainingStop;
    @FXML
    RadioMenuItem tripAverageSpeedRadioItem;
    @FXML
    RadioMenuItem tripDistanceRadioItem;
    @FXML
    TextField routesSearchTextField;
    @FXML
    TextField stopTimeTripIdTextField;
    @FXML
    TextField stopTimeSequenceNumTextField;
    @FXML
    TextField stopSearchTextField;
    @FXML
    TextField tripSearchTextField;
    @FXML
    TextArea importReportTextArea;
    @FXML
    TextArea stopTimesTextArea;
    @FXML
    TextArea routesTextArea;
    @FXML
    TextArea stopsTextArea;
    @FXML
    TextArea tripsTextArea;
    @FXML
    ListView<Route> mapRouteListView;
    @FXML
    ListView<Trip> mapTripListView;
    @FXML
    ListView<Stop> mapStopListView;
    @FXML
    TextField stopLabelTextField;
    @FXML
    GoogleMapView googleMapView;
    @FXML
    TextField mapStatDispayText;
    @FXML
    ImageView routeIconImageView;
    @FXML
    ImageView tripIconImageView;

    /**
     * List of Bus concrete observers.
     */
    public ArrayList<Bus> mBus;

    /**
     * The main FileHandler to validate/write all imported data.
     */
    public FileHandler mFileHandler;

    /**
     * The List to be displayed in the GUI.
     */
    public List mList;

    private boolean filesHaveBeenImported;

    /**
     * Initialize observers with GUI objects
     */
    @FXML
    public void initialize() {
        routesViewer = new RoutesViewer(routesTextArea);
        tripsViewer = new TripsViewer(tripsTextArea);
        stopsViewer = new StopsViewer(stopsTextArea);
        stopTimesViewer = new StopTimesViewer(stopTimesTextArea);
        gtfsGoogleMapObserver = new GTFSGoogleMapObserver(
                googleMapView,
                mapRouteListView,
                mapTripListView,
                mapStopListView,
                stopLabelTextField,
                mapStatDispayText,
                routeIconImageView,
                tripIconImageView);
        googleMapView.addMapInializedListener(this);
    }

    /**
     * Initializes the Map.
     */
    @Override
    public void mapInitialized() {
        gtfsGoogleMapObserver.initializeMap();
    }

    /**
     * Export the currently loaded GTFS files
     */
    public void exportFiles() {
        if (!filesHaveBeenImported) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Unable to Export");
            alert.setHeaderText("No Files to Export");
            alert.setContentText("Please import valid files into " +
                    "the program before attempting to export files.");
            alert.showAndWait();
            return;
        }
        try {
            File saveLocation = new File("./ExportFiles");
            fileHandler.exportGTFSData(saveLocation.toPath());

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Success");
            alert.setHeaderText("Export Successful");
            alert.setContentText(
                    "The files were successfully exported to the following location: " +
                    saveLocation.getCanonicalPath());
            alert.showAndWait();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Unable to Export");
            alert.setHeaderText("Error During Exporting");
            alert.setContentText("An unknown error occurred while exporting.");
            alert.showAndWait();
        }
    }

    /**
     * Display window to allow user to import a GTFS file
     * Display a confirmation if overwriting current files.
     */
    @FXML
    public void importFiles() {
        try {
            // Check if files have previously been imported.
            // If so, user confirms whether they want to overwrite or not.
            if (filesHaveBeenImported) {
                //Show Confirmation Dialog
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Overwrite Confirmation");
                alert.setHeaderText("Existing Import Files will be Overwritten");
                alert.setContentText("Files have already been imported. " +
                        "By importing new files, the current ones will be overwritten. " +
                        "Do you wish to continue?");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.CANCEL) { // User chose CANCEL or closed the dialog.
                    return;
                } // If user chose OK, continue the import process below.
            }

            // Refresh the file handler
            fileHandler = new FileHandler();
            fileHandler.getGtfsData().addObserver(routesViewer);
            fileHandler.getGtfsData().addObserver(stopsViewer);
            fileHandler.getGtfsData().addObserver(tripsViewer);
            fileHandler.getGtfsData().addObserver(stopTimesViewer);

            FXMLLoader loader =
                    new FXMLLoader(getClass().getResource("../fxmlFiles/import_menu.fxml"));
            Parent root = loader.load(); //calls ImportMenuController's constructor
            Stage importStage = new Stage();

            importMenuController = loader.getController();
            importMenuController.setFileHandler(fileHandler);
            importMenuController.setStage(importStage);
            importMenuController.setImportReportTextArea(importReportTextArea);

            importStage.setScene(new Scene(root));
            importStage.setTitle("Import GTFS Files");
            importStage.showAndWait();

            if (importMenuController.getImportSuccessful()) {
                filesHaveBeenImported = true; // else keep current state
                gtfsData = fileHandler.getGtfsData();
            }
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("GUI Error");
            alert.setHeaderText("FXML Error");
            alert.setContentText("JavaFX encountered an error " +
                    "when loading the ImportMenu FXML file.");
            alert.showAndWait();
        }
    }

    @FXML
    public void updateRoute() {
        try {
            if (!filesHaveBeenImported) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("No Files Imported");
                alert.setHeaderText("No Files Imported");
                alert.setContentText("Please import files before attempting to update any attributes");
                alert.showAndWait();
                return;
            }

            Route targetRoute = gtfsData.getRoute(routesSearchTextField.getText());
            if (targetRoute == null) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Invalid Input");
                alert.setHeaderText("Invalid Route ID");
                alert.setContentText("That Route ID does not match any imported routes.");
                alert.showAndWait();
                return;
            }

            FXMLLoader loader =
                    new FXMLLoader(getClass().getResource("../fxmlFiles/updateMenus/update_route_menu.fxml"));
            Parent root = loader.load();
            Stage routeUpdateStage = new Stage();

            updateRouteMenuController = loader.getController();
            updateRouteMenuController.setStage(routeUpdateStage);
            updateRouteMenuController.setRoute(targetRoute);
            updateRouteMenuController.initializeTextFields();

            routeUpdateStage.setScene(new Scene(root));
            routeUpdateStage.setTitle("Update Route");
            routeUpdateStage.showAndWait();

            if (!updateRouteMenuController.getRoute().equals(targetRoute)) {
                gtfsData.removeRoute(targetRoute);
                gtfsData.addRoute(updateRouteMenuController.getRoute());
            }

            gtfsData.notifyObservers();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("GUI Error");
            alert.setHeaderText("FXML Error");
            alert.setContentText("JavaFX encountered an error " +
                    "when loading the ImportMenu FXML file.");
            alert.showAndWait();
        }
    }

    @FXML
    public void updateStop() {
        try {
            if (!filesHaveBeenImported) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("No Files Imported");
                alert.setHeaderText("No Files Imported");
                alert.setContentText("Please import files before attempting to update any attributes");
                alert.showAndWait();
                return;
            }

            Stop targetStop = gtfsData.getStop(stopSearchTextField.getText());
            if (targetStop == null) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Invalid Input");
                alert.setHeaderText("Invalid Stop ID");
                alert.setContentText("That Stop ID does not match any imported stops.");
                alert.showAndWait();
                return;
            }

            FXMLLoader loader =
                    new FXMLLoader(getClass().getResource("../fxmlFiles/updateMenus/update_stop_menu.fxml"));
            Parent root = loader.load();
            Stage stopUpdateStage = new Stage();

            updateStopMenuController = loader.getController();
            updateStopMenuController.setStage(stopUpdateStage);
            updateStopMenuController.setStop(targetStop);
            updateStopMenuController.initializeTextFields();

            stopUpdateStage.setScene(new Scene(root));
            stopUpdateStage.setTitle("Update Stop");
            stopUpdateStage.showAndWait();

            if (!updateStopMenuController.getStop().equals(targetStop)) {
                gtfsData.removeStop(targetStop);
                gtfsData.addStop(updateStopMenuController.getStop());
            }

            gtfsData.notifyObservers();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("GUI Error");
            alert.setHeaderText("FXML Error");
            alert.setContentText("JavaFX encountered an error " +
                    "when loading the ImportMenu FXML file.");
            alert.showAndWait();
        }
    }

    @FXML
    public void updateStopTime() {
        try {
            if (!filesHaveBeenImported) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("No Files Imported");
                alert.setHeaderText("No Files Imported");
                alert.setContentText("Please import files before attempting to update any attributes");
                alert.showAndWait();
                return;
            }

            StopTime targetStopTime = gtfsData.getStopTime(stopTimeTripIdTextField.getText(), Integer.parseInt(stopTimeSequenceNumTextField.getText()));
            if (targetStopTime == null) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Invalid Input");
                alert.setHeaderText("Invalid Trip ID or Sequence Number");
                alert.setContentText("That Trip ID and Sequence Number pair does not match any imported stop times.");
                alert.showAndWait();
                return;
            }

            FXMLLoader loader =
                    new FXMLLoader(getClass().getResource("../fxmlFiles/updateMenus/update_stop_time_menu.fxml"));
            Parent root = loader.load();
            Stage stopTimeUpdateStage = new Stage();

            updateStopTimeMenuController = loader.getController();
            updateStopTimeMenuController.setStage(stopTimeUpdateStage);
            updateStopTimeMenuController.setStopTime(targetStopTime);
            updateStopTimeMenuController.initializeTextFields();

            stopTimeUpdateStage.setScene(new Scene(root));
            stopTimeUpdateStage.setTitle("Update Stop Time");
            stopTimeUpdateStage.showAndWait();

            if (!updateStopTimeMenuController.getStopTime().equals(targetStopTime)) {
                gtfsData.removeStopTime(targetStopTime);
                gtfsData.addStopTime(updateStopTimeMenuController.getStopTime());
            }

            gtfsData.notifyObservers();
        } catch (NumberFormatException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Invalid Input");
            alert.setHeaderText("Invalid Sequence Number");
            alert.setContentText("Please enter a non-negative integer for the sequence number.");
            alert.showAndWait();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("GUI Error");
            alert.setHeaderText("FXML Error");
            alert.setContentText("JavaFX encountered an error " +
                    "when loading the ImportMenu FXML file.");
            alert.showAndWait();
        }
    }

    @FXML
    public void updateTrip() {
        try {
            if (!filesHaveBeenImported) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("No Files Imported");
                alert.setHeaderText("No Files Imported");
                alert.setContentText("Please import files before attempting to update any attributes");
                alert.showAndWait();
                return;
            }

            Trip targetTrip = gtfsData.getTrip(tripSearchTextField.getText());
            if (targetTrip == null) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Invalid Input");
                alert.setHeaderText("Invalid Trip ID");
                alert.setContentText("That Trip ID does not match any imported trips.");
                alert.showAndWait();
                return;
            }

            FXMLLoader loader =
                    new FXMLLoader(getClass().getResource("../fxmlFiles/updateMenus/update_trip_menu.fxml"));
            Parent root = loader.load();
            Stage tripUpdateStage = new Stage();

            updateTripMenuController = loader.getController();
            updateTripMenuController.setStage(tripUpdateStage);
            updateTripMenuController.setTrip(targetTrip);
            updateTripMenuController.initializeTextFields();

            tripUpdateStage.setScene(new Scene(root));
            tripUpdateStage.setTitle("Update Trip");
            tripUpdateStage.showAndWait();

            if (!updateTripMenuController.getTrip().equals(targetTrip)) {
                gtfsData.removeTrip(targetTrip);
                gtfsData.addTrip(updateTripMenuController.getTrip());
            }

            gtfsData.notifyObservers();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("GUI Error");
            alert.setHeaderText("FXML Error");
            alert.setContentText("JavaFX encountered an error " +
                    "when loading the ImportMenu FXML file.");
            alert.showAndWait();
        }
    }

    /**
     * Searches for a Trip
     */
    @FXML
    public void searchTrips() {
        List<MenuItem> menuItems = tripsSearchTarget.getItems();

        try {
            for (MenuItem item : menuItems) {
                RadioMenuItem radioItem = (RadioMenuItem) item;
                if (radioItem.isSelected()) {
                    if (radioItem.equals(tripAverageSpeedRadioItem)) {
                        double averageSpeed =
                                fileHandler.getGtfsData().
                                        getTrip(tripSearchTextField.getText()).getAverageSpeed();
                        tripsViewer.setText("Average speed for trip with id \"" +
                                tripSearchTextField.getText() + "\": \"" + averageSpeed + "mph\"");
                    } else if (radioItem.equals(tripDistanceRadioItem)) {
                        double tripDistance =
                                fileHandler.getGtfsData().
                                        getTrip(tripSearchTextField.getText()).getTotalDistance();
                        tripsViewer.setText("Distance for trip with id \"" +
                                tripSearchTextField.getText() + "\": \"" + tripDistance + "mi\"");
                    }
                }
            }
        } catch (NullPointerException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Unable to Match Trip to Trip ID");
            alert.setContentText("The stop ID that you entered does not match any trips.");
            alert.showAndWait();
        }
    }

    /**
     * Searches for a Stop
     */
    @FXML
    public void searchStops() {
        List<MenuItem> menuItems = stopsSearchTarget.getItems();
        try {
            for (MenuItem item : menuItems) {
                RadioMenuItem radioItem = (RadioMenuItem) item;
                if (radioItem.isSelected()) {
                    if (radioItem.equals(stopNextTripRadioButton)) {
                        String nextTrip = gtfsData.getStop(
                                stopSearchTextField.getText()).getNextTrip().getTripId();
                        stopsViewer.setText("Next trip stopping at stop with stop id \""
                                + stopSearchTextField.getText()
                                + "\" has the id of: \"" + nextTrip + "\"");
                    } else if (radioItem.equals(stopRoutesContainingStopButton)) {
                        displayRouteIdsContainingStop();
                    } else if (radioItem.equals(stopTripsContainingStop)) {
                        displayNumTrips();
                    }
                }
            }
        } catch (NullPointerException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Unable to Match Stop to Stop ID");
            alert.setContentText("The stop ID that you entered does not match any stops.");
            alert.showAndWait();
        }
    }

    /**
     * Displays the route_ids that are associated with the passed in stop_id
     * onto the GUI.
     */
    private void displayRouteIdsContainingStop() {
        List<Trip> tripsContainingStopId = new ArrayList<>();
        String stopId = stopSearchTextField.getText();
        List<StopTime> stopTimes = gtfsData.getStopTimes();
        for (StopTime stopTime : stopTimes) {
            if (stopTime.getStopId().equals(stopId)) {
                Trip trip = gtfsData.getTrip(stopTime.getTripId());
                if (!tripsContainingStopId.contains(trip)) {
                    tripsContainingStopId.add(trip);
                }
            }
        }

        List<Route> routesWithStopId = new ArrayList<>();
        for (Trip trip : tripsContainingStopId) {
            for (Route route : gtfsData.getRoutes()) {
                if (route.getRouteId().equals(trip.getRouteId())) {
                    Route tempRoute = gtfsData.getRoute(trip.getRouteId());
                    if (!routesWithStopId.contains(tempRoute)) {
                        routesWithStopId.add(tempRoute);
                    }
                }
            }
        }
        StringBuilder routeIdString = new StringBuilder();
        for (Route route : routesWithStopId) {
            routeIdString.append(route.getRouteId()).append("\r\n");
        }
        stopsViewer.setText("Route_Ids that contain the stop_id: " + stopId +
                " are listed below\r\n" + routeIdString);
    }

    /**
     * Displays both the number of trips that go through a stop ands the ID of the trips themselves
     */
    private void displayNumTrips() {
        String stopId = stopSearchTextField.getText();
        Stop stop = gtfsData.getStop(stopId);
        LinkedList<String> tripsIds = stop.getTripsOnStop();
        StringBuilder text = new StringBuilder(
                "Number of Trips on Stop: " + tripsIds.size() + "\n");
        if (!tripsIds.isEmpty()) {
            for (String tripId: tripsIds) {
                text.append(tripId).append("\n");
            }
        }
        stopsViewer.setText(text.toString());
    }

    /**
     * Update bus locations in GTFS files
     */
    public void updateBusLocations() {

    }

    /**
     * Update the display GUIs
     */
    public void updateDisplay() {

    }

    /**
     * Display a list of all stops
     */
    public void displayStops() {

    }

    /**
     * Display the distance of the given trip
     *
     * @param tripId - ID of trip to display the distance of
     * @return the trip distance as an int
     */
    public int displayTripDistance(int tripId) {
        return 0;
    }

    /**
     * Display the average speed of the given trip
     */
    public void displayTripAvgSpeed() {

    }

    /**
     * Display the information of a given route
     *
     * @param routeId - ID of the route to display the info of
     */
    public void displayRoute(int routeId) {

    }

    /**
     * Display the next trip to arrive at a stop
     *
     * @param stopId - ID of the stop of the stop to check
     * @return the Trip object of the next trip.
     */
    public Trip displayNextTripFromStop(int stopId) {
        return null;
    }

    /**
     * Display the map
     */
    public void displayMap() {

    }

    /**
     * Plot the buses for the given route
     *
     * @param routeId - ID of the route with buses to plot
     */
    public void plotBuses(int routeId) {

    }

    /**
     * Change the location of a given stop
     *
     * @param stopId - ID of the stop to change
     */
    public void changeStopLocation(int stopId) {

    }

    /**
     * Change the stop time for a given stop
     *
     * @param stopId - ID of the stop to change
     */
    public void changeStopTime(int stopId) {

    }

    /**
     * Display similar trips
     */
    public void displaySimilarTrips() {

    }
}

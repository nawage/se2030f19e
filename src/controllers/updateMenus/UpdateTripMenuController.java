/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package controllers.updateMenus;

import dataManagement.Trip;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controller class for the 'update_trip_menu.fxml' file.
 *
 * @author Noah Villanueva
 * @created 10-Nov-2019 1∶10∶41 PM
 */
public class UpdateTripMenuController {
    // Private variables
    private Trip trip;
    private Stage stage;

    // FXML variables
    @FXML
    TextField routeId;
    @FXML
    TextField serviceId;
    @FXML
    TextField tripId;
    @FXML
    TextField tripHeadsign;
    @FXML
    TextField directionId;
    @FXML
    TextField blockId;
    @FXML
    TextField shapeId;
    @FXML
    Button saveButton;
    @FXML
    Button cancelButton;

    /**
     * Set the text for all text fields
     */
    public void initializeTextFields() {
        routeId.setText(trip.getRouteId());
        serviceId.setText(trip.getServiceId());
        tripId.setText(trip.getTripId());
        tripHeadsign.setText(trip.getTripHeadSign());
        directionId.setText(trip.getDirectionId());
        blockId.setText(trip.getBlockId());
        shapeId.setText(String.valueOf(trip.getShapeId()));
    }

    /**
     * Update the stopTime with the new data
     */
    @FXML
    public void save() {
        trip = new Trip(routeId.getText(), serviceId.getText(), tripId.getText(), tripHeadsign.getText(), directionId.getText(), blockId.getText(), shapeId.getText());
        close();
    }

    /**
     * Close the window without saving
     */
    @FXML
    public void close() {
        stage.close();
    }

    /**
     * Getter for trip
     * @return trip
     */
    public Trip getTrip() {
        return trip;
    }

    /**
     * Setter for trip
     * @param trip - New trip
     */
    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    /**
     * Setter for stage
     * @param stage - New stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }
}

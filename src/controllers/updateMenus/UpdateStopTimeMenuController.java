/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package controllers.updateMenus;

import dataManagement.StopTime;
import fileManagement.validators.NonNegativeIntegerValidator;
import fileManagement.validators.TimeValidator;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controller class for the 'update_stopTime_time_menu.fxml' file.
 *
 * @author Noah Villanueva
 * @created 10-Nov-2019 1∶05∶12 PM
 */
public class UpdateStopTimeMenuController {
    // Private variables
    private StopTime stopTime;
    private Stage stage;

    // FXML variables
    @FXML
    TextField tripId;
    @FXML
    TextField arrivalTime;
    @FXML
    TextField departureTime;
    @FXML
    TextField stopId;
    @FXML
    TextField stopSequence;
    @FXML
    TextField stopHeadsign;
    @FXML
    TextField pickupType;
    @FXML
    TextField dropOffType;
    @FXML
    Button saveButton;
    @FXML
    Button cancelButton;

    /**
     * Set the text for all text fields
     */
    public void initializeTextFields() {
        tripId.setText(stopTime.getTripId());
        arrivalTime.setText(stopTime.getArrivalTime().toString());
        departureTime.setText(stopTime.getDepartureTime().toString());
        stopId.setText(String.valueOf(stopTime.getStopId()));
        stopSequence.setText(String.valueOf(stopTime.getStopSequence()));
        stopHeadsign.setText(stopTime.getStopHeadSign());
        pickupType.setText(String.valueOf(stopTime.getPickupType()));
        dropOffType.setText(String.valueOf(stopTime.getDropOffType()));
    }

    /**
     * Update the stopTime with the new data
     */
    @FXML
    public void save() {
        TimeValidator timeValidator = new TimeValidator();
        NonNegativeIntegerValidator intValidator = new NonNegativeIntegerValidator();

        if (timeValidator.isValid(arrivalTime.getText()) && timeValidator.isValid(departureTime.getText())) {
            if (intValidator.isValid(pickupType.getText()) && intValidator.isValid(dropOffType.getText())) {
                stopTime = new StopTime(tripId.getText(), arrivalTime.getText(), departureTime.getText(),
                        stopId.getText(), Integer.parseInt(stopSequence.getText()), stopHeadsign.getText(),
                        Integer.parseInt(pickupType.getText()), Integer.parseInt(dropOffType.getText()));
                close();
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Invalid Input");
                alert.setHeaderText("Invalid Pickup or Drop Off Type");
                alert.setContentText("Please ensure that both the pickup and drop off types are valid.");
                alert.showAndWait();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Invalid Input");
            alert.setHeaderText("Invalid Arrival or Departure Time");
            alert.setContentText("Please ensure that both the arrival and departure times are valid.");
            alert.showAndWait();
        }

    }

    /**
     * Close the window without saving
     */
    @FXML
    public void close() {
        stage.close();
    }

    /**
     * Getter for stopTime
     * @return stopTime
     */
    public StopTime getStopTime() {
        return stopTime;
    }

    /**
     * Setter for stopTime
     * @param stopTime - New stopTime
     */
    public void setStopTime(StopTime stopTime) {
        this.stopTime = stopTime;
    }

    /**
     * Setter for stage
     * @param stage - New stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }
}

/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package controllers.updateMenus;

import dataManagement.Route;
import fileManagement.validators.NonNegativeIntegerValidator;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controller class for the 'update_route_menu.fxml' file.
 *
 * @author Noah Villanueva
 * @created 10-Nov-2019 12∶54∶51 AM
 */
public class UpdateRouteMenuController {
    // Private variables
    private Route route;
    private Stage stage;

    // FXML variables
    @FXML
    TextField routeId;
    @FXML
    TextField agencyId;
    @FXML
    TextField routeShortName;
    @FXML
    TextField routeLongName;
    @FXML
    TextField routeDesc;
    @FXML
    TextField routeType;
    @FXML
    TextField routeUrl;
    @FXML
    TextField routeColor;
    @FXML
    TextField routeTextColor;
    @FXML
    Button saveButton;
    @FXML
    Button cancelButton;

    /**
     * Set the text for all text fields
     */
    public void initializeTextFields() {
        routeId.setText(route.getRouteId());
        agencyId.setText(route.getAgencyId());
        routeShortName.setText(route.getRouteShortName());
        routeLongName.setText(route.getRouteLongName());
        routeDesc.setText(route.getRouteDescription());
        routeType.setText(String.valueOf(route.getRouteType()));
        routeUrl.setText(route.getRouteUrl());
        routeColor.setText(route.getRouteColor());
        routeTextColor.setText(route.getRouteTextColor());
    }

    /**
     * Update the route with the new data
     */
    @FXML
    public void save() {
        try {
            NonNegativeIntegerValidator intValidator = new NonNegativeIntegerValidator();

            if (intValidator.isValid(routeType.getText())) {
                int routeTypeInt = Integer.parseInt(routeType.getText());

                if ((routeTypeInt >= 0) && (routeTypeInt <= 7)) {
                    route = new Route(routeId.getText(), agencyId.getText(), routeShortName.getText(), routeLongName.getText(),
                            routeDesc.getText(), Integer.parseInt(routeType.getText()), routeUrl.getText(), routeColor.getText(),
                            routeTextColor.getText());
                    close();
                }
                else {
                    throw new NumberFormatException();
                }
            } else {
                throw new NumberFormatException();
            }
        } catch (NumberFormatException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Invalid Input");
            alert.setHeaderText("Invalid Route Type");
            alert.setContentText("'Route Type' field must be a non-negative integer 0 - 7.'");
            alert.showAndWait();
        }
    }

    /**
     * Close the window without saving
     */
    @FXML
    public void close() {
        stage.close();
    }

    /**
     * Getter for route
     * @return route
     */
    public Route getRoute() {
        return route;
    }

    /**
     * Setter for route
     * @param route - New route
     */
    public void setRoute(Route route) {
        this.route = route;
    }

    /**
     * Setter for stage
     * @param stage - New stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }
}

/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package fileManagement;

import dataManagement.Route;
import dataManagement.Stop;
import dataManagement.StopTime;
import dataManagement.Trip;
import fileManagement.verifiers.RouteVerifier;
import fileManagement.verifiers.StopTimeVerifier;
import fileManagement.verifiers.StopVerifier;
import fileManagement.verifiers.TripVerifier;
import subject.GTFSData;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

/**
 * Handles validation of GTFS files and writing of data to GTFS Data structure from files.
 *
 * @author Nathan Wagenknecht
 * @version 1.0
 * @created 10-Oct-2019 9:10:31 AM
 */
public class FileHandler {

    /**
     * Stores GTFS Data in a custom data structure.
     */
    private GTFSData gtfsData;

    /**
     * The valid header line for a Route file.
     */
    public static final List<String> ROUTES_HEADER_LINE =
            Arrays.asList("route_id", "agency_id", "route_short_name",
                    "route_long_name", "route_desc", "route_type",
                    "route_url", "route_color", "route_text_color");

    /**
     * The valid header line for a Stop file.
     */
    public static final List<String> STOPS_HEADER_LINE =
            Arrays.asList("stop_id", "stop_name", "stop_desc", "stop_lat", "stop_lon");

    /**
     * The valid header line for a Trip file.
     */
    public static final List<String> TRIPS_HEADER_LINE =
            Arrays.asList("route_id", "service_id", "trip_id",
                    "trip_headsign", "direction_id", "block_id", "shape_id");

    /**
     * The valid header line for a StopTime file.
     */
    public static final List<String> STOP_TIMES_HEADER_LINE =
            Arrays.asList("trip_id", "arrival_time",
                    "departure_time", "stop_id", "stop_sequence",
                    "stop_headsign", "pickup_type", "drop_off_type");

    /**
     * FileHandler Constructor
     */
    public FileHandler() {
        this.gtfsData = GTFSData.getInstance();
    }

    /**
     * Constructs GTFSCSVParser objects to process GTFS files.
     * Only changes GTFSData if the entire filegroup is valid.
     *
     * @param files collection of GTFS files to pull data from
     * @return ImportReport containing file verification information
     * @throws FileNotFoundException thrown if one of the GTFSFiles cannot be found
     */
    public ImportReport importGTFSData(GTFSFiles files) throws FileNotFoundException {

        ImportReport importReport = new ImportReport();

        //-------------------- Validate & Process Routes File
        GTFSCSVParser<Route> routeCSVParser =
                new GTFSCSVParser<>(Route.class, new RouteVerifier<>(), ROUTES_HEADER_LINE);
        List<Route> routes = routeCSVParser.parseCSVFile(files.getRoutesFile());
        importReport.setRoutesImportReport(routeCSVParser.getParserReport());
        importReport.setRoutesFileValid(routeCSVParser.isFileValid());
        //-------------------- Validate & Process Stops File
        GTFSCSVParser<Stop> stopCSVParser =
                new GTFSCSVParser<>(Stop.class, new StopVerifier<>(), STOPS_HEADER_LINE);
        List<Stop> stops = stopCSVParser.parseCSVFile(files.getStopsFile());
        importReport.setStopsImportReport(stopCSVParser.getParserReport());
        importReport.setStopsFileValid(stopCSVParser.isFileValid());
        //-------------------- Validate & Process Trips File
        GTFSCSVParser<Trip> tripCSVParser =
                new GTFSCSVParser<>(Trip.class, new TripVerifier<>(), TRIPS_HEADER_LINE);
        List<Trip> trips = tripCSVParser.parseCSVFile(files.getTripsFile());
        importReport.setTripsImportReport(tripCSVParser.getParserReport());
        importReport.setTripsFileValid(tripCSVParser.isFileValid());
        //-------------------- Validate & Process StopTimes File
        GTFSCSVParser<StopTime> stopTimeCSVParser =
                new GTFSCSVParser<>(
                        StopTime.class, new StopTimeVerifier<>(), STOP_TIMES_HEADER_LINE);
        List<StopTime> stopTimes = stopTimeCSVParser.parseCSVFile(files.getStopTimesFile());
        importReport.setStopTimesImportReport(stopTimeCSVParser.getParserReport());
        importReport.setStopTimesFileValid(stopTimeCSVParser.isFileValid());


        //Process changes if the report shows that each file is minimally valid
        if (importReport.isFileGroupValid()) {

            //clear data in GTFSData
            gtfsData.clear();

            //add new route, stop, trip, and stop time objects to clean GTFSData
            gtfsData.addRoutes(routes);
            gtfsData.addStops(stops);
            gtfsData.addTrips(trips);
            gtfsData.addStopTimes(stopTimes);
        }

        return importReport;
    }

    /**
     * Exports GTFSData as 4 separate csv files at specified paths.
     *
     * @param path directory path to create files in
     * @return whether all 4 files were exported successfully
     * @throws IOException if file not found
     */
    public boolean exportGTFSData(Path path) throws IOException {
        if (!path.toFile().exists()) {
            new File(path.toString()).mkdir();
        }

        long nanoTime = System.nanoTime();

        GTFSCSVWriter<Route> routeGTFSCSVWriter = new GTFSCSVWriter<>(gtfsData.getRoutes());
        routeGTFSCSVWriter.writeCSVFile(path, "/e_routes_" + nanoTime + ".txt");

        GTFSCSVWriter<Stop> stopGTFSCSVWriter = new GTFSCSVWriter<>(gtfsData.getStops());
        stopGTFSCSVWriter.writeCSVFile(path, "/e_stops_" + nanoTime + ".txt");

        GTFSCSVWriter<Trip> tripGTFSCSVWriter = new GTFSCSVWriter<>(gtfsData.getTrips());
        tripGTFSCSVWriter.writeCSVFile(path, "/e_trips_" + nanoTime + ".txt");

        GTFSCSVWriter<StopTime> stopTimeGTFSCSVWriter =
                new GTFSCSVWriter<>(gtfsData.getStopTimes());
        stopTimeGTFSCSVWriter.writeCSVFile(path, "/e_stopTimes_" + nanoTime + ".txt");

        return routeGTFSCSVWriter.getWasExportSuccessful() &&
                stopGTFSCSVWriter.getWasExportSuccessful() &&
                tripGTFSCSVWriter.getWasExportSuccessful() &&
                stopTimeGTFSCSVWriter.getWasExportSuccessful();
    }

    /**
     * Getter for gtfsData.
     *
     * @return gtfsData
     */
    public GTFSData getGtfsData() {
        return gtfsData;
    }
}
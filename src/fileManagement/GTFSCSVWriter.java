/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package fileManagement;

import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.util.List;

/**
 * A CSV writer for GTFS files.
 *
 * @author Nathan Wagenkencht
 * @param <E> GTFS Object to write.
 */
public class GTFSCSVWriter<E> {
    private boolean wasExportSuccessful;
    private int numCapturedExceptions;
    private StatefulBeanToCsv<E> beanToCsv;
    private List<E> beans;
    private Path exportPath;

    /**
     * Constructor for GTFSCSVWriter.
     *
     * @param beans Data to be instantiated.
     */
    public GTFSCSVWriter(List<E> beans) {
        this.beans = beans;
    }

    /**
     * Writes a CVSFile.
     *
     * @param dirPath the direction path
     * @param filename thr file name
     * @return whether the file was written too
     * @throws IOException in case of an issue when writing
     */
    public boolean writeCSVFile(Path dirPath, String filename) throws IOException {
        //assume export is true unless it fails ay any step
        wasExportSuccessful = true;

        try (Writer writer = new FileWriter(new File(dirPath.toString() + filename))) {
            beanToCsv = new StatefulBeanToCsvBuilder<E>(writer)
                    .withThrowExceptions(false)
                    .withApplyQuotesToAll(false)
                    .withOrderedResults(true)
                    .build();
            beanToCsv.write(beans);

            for (CsvException e : beanToCsv.getCapturedExceptions()) {
                System.out.println(e.getMessage() + " on line " + e.getLineNumber());
            }
        } catch (CsvRequiredFieldEmptyException e) {
            e.printStackTrace();
        } catch (CsvDataTypeMismatchException e) {
            e.printStackTrace();
        } finally {
            numCapturedExceptions = beanToCsv.getCapturedExceptions().size();
        }

        if (numCapturedExceptions == beans.size()) {
            //no lines were exported so whole export fails
            return false;
        }

        return true;
    }

    //Getters

    /**
     * Getter for GTFSCSVWriterReport.
     *
     * @return GTFSCSVWriterReport
     */
    public String getGTFSCSVWriterReport() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("export to %s", exportPath.toString()));
        for (CsvException e : getCapturedExceptions()) {
            sb.append(String.format("Error exporting line <%d> : ",
                    e.getLineNumber(), e.getMessage()));
        }
        sb.append(":end of report");
        return sb.toString();
    }

    /**
     * Returns whether or not the export was successful.
     *
     * @return whether or not the export was successful.
     */
    public boolean getWasExportSuccessful() {
        return this.wasExportSuccessful;

    }

    /**
     * Gets captured exceptions.
     *
     * @return a List of captured exceptions.
     */
    public List<CsvException> getCapturedExceptions() {
        return beanToCsv.getCapturedExceptions();
    }

    /**
     * Gets the number of captures exceptions.
     *
     * @return the number of captures exceptions as an int.
     */
    public int getNumCapturedExceptions() {
        return beanToCsv.getCapturedExceptions().size();
    }

    /**
     * Gets the number of lines exported.
     *
     * @return the number of lines exported as an int.
     */
    public int getNumberLinesExported() {
        return beans.size() - getNumCapturedExceptions();
    }
}

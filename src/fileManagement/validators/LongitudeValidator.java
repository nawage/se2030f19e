/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package fileManagement.validators;

import com.opencsv.bean.BeanField;
import com.opencsv.bean.validators.StringValidator;
import com.opencsv.exceptions.CsvValidationException;

/**
 * Validates that the given value is a valid latitude
 *
 * @author Nathan Wagenknecht
 */
public class LongitudeValidator implements StringValidator {
    private String errorMessage;
    private static final int MAX_DEGREES = 180;
    private static final int MIN_DEGREES = -180;

    /**
     * Tests if the value is Valid.
     *
     * @param value the value being evaluated
     * @return whether the Value is valid
     */
    @Override
    public boolean isValid(String value) {
        if (value.isEmpty()) {
            return true;
        }
        try {
            if (Double.parseDouble(value) < MIN_DEGREES || Double.parseDouble(value) > MAX_DEGREES){
                errorMessage = "value outside allowable range [-180, 180]";
                return false;
            }
            return true;
        } catch(NumberFormatException e) {
            errorMessage = e.getMessage();
        }
        return false;
    }

    /**
     * Validates the Longitude value.
     *
     * @param value The longitude value being validated.
     * @param beanField the bean field
     * @throws CsvValidationException In case of an invalid longitude coordinate.
     */
    @Override
    public void validate(String value, BeanField beanField) throws CsvValidationException {
        if (!isValid(value)) {
            errorMessage = value + " is not a valid longitude coordinate";
            throw new CsvValidationException("Invalid " +
                    beanField.getField().getName() + ", " + errorMessage);
        }
    }

    //Setter
    @Override
    public void setParameterString(String s) {

    }
}
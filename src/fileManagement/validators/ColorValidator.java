/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package fileManagement.validators;

import com.opencsv.bean.BeanField;
import com.opencsv.bean.validators.StringValidator;
import com.opencsv.exceptions.CsvValidationException;
import javafx.scene.paint.Color;

/**
 * Validates that a time is a valid format.
 *
 * @author Nathan Wagenknecht
 */
public class ColorValidator implements StringValidator {
    private String errorMessage;

    /**
     * Tests if the value is Valid.
     *
     * @param value the value being evaluated
     * @return whether the Value is valid
     */
    @Override
    public boolean isValid(String value) {
        try {
            Color.web(value);
        } catch (Exception e) {
            errorMessage = e.getMessage();
            return false;
        }
        return true;
    }

    /**
     * Validates a Color value.
     *
     * @param value The Color value being validated.
     * @param beanField the bean field
     * @throws CsvValidationException in case of an invalid color.
     */
    @Override
    public void validate(String value, BeanField beanField) throws CsvValidationException {
        if (!this.isValid(value)) {
            throw new CsvValidationException(beanField.getField().getName() +
                    " " + value + " is not a valid Color: " + errorMessage);
        }
    }

    //Setter
    @Override
    public void setParameterString(String s) {

    }
}

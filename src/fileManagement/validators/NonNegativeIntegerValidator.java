/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package fileManagement.validators;

import com.opencsv.bean.BeanField;
import com.opencsv.bean.validators.StringValidator;
import com.opencsv.exceptions.CsvValidationException;

/**
 * Validates that an integer is non-negative.
 *
 * @author Nathan Wagenknecht
 */
public class NonNegativeIntegerValidator implements StringValidator {
    private String errorMessage;

    /**
     * Tests if the value is Valid.
     *
     * @param value the value being evaluated
     * @return whether the Value is valid
     */
    @Override
    public boolean isValid(String value) {
        try {
            return Integer.parseInt(value) >= 0;
        } catch(NumberFormatException e) {
            errorMessage = e.getMessage();
        }
        return false;
    }

    /**
     * Validates an integer value to ensure a non-negative integer.
     *
     * @param value whats being validated
     * @param beanField the bean field
     * @throws CsvValidationException In case of a non-negative integer.
     */
    @Override
    public void validate(String value, BeanField beanField) throws CsvValidationException {
        if (!isValid(value)) {
            errorMessage = value + " is a negative integer (expected positive or zero)";
            throw new CsvValidationException("Invalid " +
                    beanField.getField().getName() + ", " + errorMessage);
        }
    }

    //Setter
    @Override
    public void setParameterString(String s) {
    }
}

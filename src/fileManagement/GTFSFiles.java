package fileManagement;

import java.io.File;

/**
 * A collection of GTFS files.
 *
 * @author Nathan Wagenkencht
 */
public class GTFSFiles {

    private File routesFile;
    private File stopTimesFile;
    private File stopsFile;
    private File tripsFile;

    /**
     * Constructor for the GTFSFiles object
     *
     * @param routesFile    routesFile file
     * @param stopTimesFile stopTimesFile file
     * @param stopsFile     stopsFile file
     * @param tripsFile     tripsFile file
     */
    public GTFSFiles(File routesFile, File stopTimesFile, File stopsFile, File tripsFile) {
        this.routesFile = routesFile;
        this.stopTimesFile = stopTimesFile;
        this.stopsFile = stopsFile;
        this.tripsFile = tripsFile;
    }

    //Getters
    public File getRoutesFile() {
        return routesFile;
    }

    public File getStopTimesFile() {
        return stopTimesFile;
    }

    public File getStopsFile() {
        return stopsFile;
    }

    public File getTripsFile() {
        return tripsFile;
    }
}

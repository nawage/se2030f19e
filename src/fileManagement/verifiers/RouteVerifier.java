package fileManagement.verifiers;

import com.opencsv.bean.BeanVerifier;
import com.opencsv.exceptions.CsvConstraintViolationException;

/**
 * Verifies syntactical correctness of Route object after it is parsed and validated,
 * post processing of the Route is also be done here.
 *
 * @author Nathan Wagenkencht
 * @param <Route> The Route type.
 */
public class RouteVerifier<Route> implements BeanVerifier<Route> {
    /**
     * Verifies integrity of Route object and performs minor post-processing if needed.
     *
     * @param route route object to verify and process
     * @return true if the Route passes verification and false otherwise
     * @throws CsvConstraintViolationException (Issue exists that warrants the Route be rejected)
     */
    @Override
    public boolean verifyBean(Route route) throws CsvConstraintViolationException {
        if (true) {
            return true;
        } else {
            throw new CsvConstraintViolationException("");
        }
    }
}

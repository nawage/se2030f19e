package fileManagement.verifiers;

import com.opencsv.bean.BeanVerifier;
import com.opencsv.exceptions.CsvConstraintViolationException;

/**
 * Verifies syntactical correctness of Trip object after it is parsed and validated,
 * post processing of the Trip is also be done here.
 *
 * @author Nathan Wagenknecht
 * @param <Trip> The Trip type.
 */
public class TripVerifier<Trip> implements BeanVerifier<Trip> {
    /**
     * Verifies integrity of Trip object and performs minor post-processing if needed.
     *
     * @param trip object to verify and process
     * @return true if the Trip passes verification and false otherwise
     * @throws CsvConstraintViolationException (Issue exists that warrants the Trip be rejected)
     */
    @Override
    public boolean verifyBean(Trip trip) throws CsvConstraintViolationException {
        if (true) {
            return true;
        } else {
            throw new CsvConstraintViolationException("");
        }
    }
}

package fileManagement.verifiers;

import com.opencsv.bean.BeanVerifier;
import com.opencsv.exceptions.CsvConstraintViolationException;

/**
 * Verifies syntactical correctness of Stop object after it is parsed and validated,
 * post processing of the Stop is also be done here.
 *
 * @author Nathan Wagenknecht
 * @param <Stop> The Stop type.
 */
public class StopVerifier<Stop> implements BeanVerifier<Stop> {
    /**
     * Verifies integrity of Stop object and performs minor post-processing if needed.
     *
     * @param  stop object to verify and process
     * @return true if the Stop passes verification and false otherwise
     * @throws CsvConstraintViolationException (Issue that warrants the Stop be rejected)
     */
    @Override
    public boolean verifyBean(Stop stop) throws CsvConstraintViolationException {
        dataManagement.Stop tempStop = (dataManagement.Stop) stop;

        double latitude = tempStop.getStopLat();
        double longitude = tempStop.getStopLon();

        if (latitude == 0 && longitude != 0) {
            throw new CsvConstraintViolationException("latitude required with longitude value");
        }

        if (latitude != 0 && longitude == 0) {
            throw new CsvConstraintViolationException("longitude required with latitude value");
        }
        return true;
    }
}

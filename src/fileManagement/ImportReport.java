package fileManagement;

/**
 * A report of file validity and line errors created after a CSV parsing/validation operation.
 *
 * @author Nathan Wagenkencht
 */
public class ImportReport {
    private String routesImportReport;
    private String stopsImportReport;
    private String tripsImportReport;
    private String stopTimesImportReport;
    private boolean isRoutesFileValid;
    private boolean isStopsFileValid;
    private boolean isTripsFileValid;
    private boolean isStopTimesFileValid;

    /**
     * Returns the validity of the file group as a whole.
     *
     * @return validity of file group
     */
    public boolean isFileGroupValid() {
        return isRoutesFileValid &&
                isStopsFileValid &&
                isTripsFileValid &&
                isStopTimesFileValid;
    }


    //Getters and Seeters
    public String getReportMessage() {
        return routesImportReport + stopsImportReport + tripsImportReport + stopTimesImportReport;
    }

    public String getRoutesImportReport() {
        return routesImportReport;
    }

    public void setRoutesImportReport(String routesImportReport) {
        this.routesImportReport = routesImportReport;
    }

    public String getStopsImportReport() {
        return stopsImportReport;
    }

    public void setStopsImportReport(String stopsImportReport) {
        this.stopsImportReport = stopsImportReport;
    }

    public String getTripsImportReport() {
        return tripsImportReport;
    }

    public void setTripsImportReport(String tripsImportReport) {
        this.tripsImportReport = tripsImportReport;
    }

    public String getStopTimesImportReport() {
        return stopTimesImportReport;
    }

    public void setStopTimesImportReport(String stopTimesImportReport) {
        this.stopTimesImportReport = stopTimesImportReport;
    }

    public boolean isRoutesFileValid() {
        return isRoutesFileValid;
    }

    public void setRoutesFileValid(boolean routesFileValid) {
        isRoutesFileValid = routesFileValid;
    }

    public boolean isStopsFileValid() {
        return isStopsFileValid;
    }

    public void setStopsFileValid(boolean stopsFileValid) {
        isStopsFileValid = stopsFileValid;
    }

    public boolean isTripsFileValid() {
        return isTripsFileValid;
    }

    public void setTripsFileValid(boolean tripsFileValid) {
        isTripsFileValid = tripsFileValid;
    }

    public boolean isStopTimesFileValid() {
        return isStopTimesFileValid;
    }

    public void setStopTimesFileValid(boolean stopTimesFileValid) {
        isStopTimesFileValid = stopTimesFileValid;
    }
}

/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package observers.viewers;

import dataManagement.Route;
import javafx.scene.control.TextArea;
import observers.Observer;
import subject.GTFSData;

import java.util.HashMap;

/**
 * Observer that allows for Viewing Routes.
 *
 * @author Noah Villanueva
 * @version 1.0
 * @version 3-Nov-2019 12:40:13 PM
 */
public class RoutesViewer implements Observer {
    private HashMap<String, Route> routes;
    private TextArea textArea;

    /**
     * Constructor for RoutesViewer.
     *
     * @param textArea - TextArea to print data to
     */
    public RoutesViewer(TextArea textArea) {
        this.textArea = textArea;
    }

    /**
     * Pull stop data from the specified subject and update routes.
     *
     * @param gtfsData - Subject to pull data from
     */
    @Override
    public void pull(GTFSData gtfsData) {
        routes = gtfsData.getRoutesHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("Route IDs:\n");

        for (Route stop : routes.values()) {
            builder.append(stop.getRouteId()).append("\n");
        }

        setText(builder.toString());
    }

    //Getters and Setters

    /**
     * Get a list of routes.
     *
     * @return - list of routes
     */
    public HashMap<String, Route> getRoutes() {
        return routes;
    }

    /**
     * Get a stop from routes.
     *
     * @param stopId - stopId of the trip to retrieve
     * @return - Trip with stopId
     */
    public Route getRoute(String stopId) {
        return routes.get(stopId);
    }

    /**
     * Prints data to the TextArea.
     *
     * @param text The text to display.
     */
    public void setText(String text) {
        textArea.setText(text);
    }
}

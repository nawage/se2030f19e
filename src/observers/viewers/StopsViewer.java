/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package observers.viewers;

import dataManagement.Stop;
import javafx.scene.control.TextArea;
import observers.Observer;
import subject.GTFSData;

import java.util.HashMap;

/**
 * Observer for Viewing Stops.
 *
 * @author Noah Villanueva
 * @version 1.0
 * @version 27-Oct-2019 6:56:18 PM
 */
public class StopsViewer implements Observer {
    private HashMap<String, Stop> stops;
    private TextArea textArea;

    /**
     * Constructor for StopsViewer.
     *
     * @param textArea - TextArea to print data to
     */
    public StopsViewer(TextArea textArea) {
        this.textArea = textArea;
    }

    /**
     * Pull stop data from the specified subject and update stops.
     *
     * @param gtfsData - Subject to pull data from
     */
    @Override
    public void pull(GTFSData gtfsData) {
        stops = gtfsData.getStopsHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("Stop IDs:\n");

        for (Stop stop : stops.values()) {
            builder.append(stop.getStopId()).append("\n");
        }

        setText(builder.toString());
    }

    //Getters and Setters

    /**
     * Get a list of stops.
     *
     * @return - list of stops
     */
    public HashMap<String, Stop> getStops() {
        return stops;
    }

    /**
     * Get a stop from stops.
     *
     * @param stopId - stopId of the trip to retrieve
     * @return - Trip with stopId
     */
    public Stop getStop(String stopId) {
        return stops.get(stopId);
    }

    /**
     * Prints data to the TextArea.
     *
     * @param text The text to display.
     */
    public void setText(String text) {
        textArea.setText(text);
    }
}

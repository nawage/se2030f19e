/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package observers;

import subject.GTFSData;

/**
 * The Observer Interface necessary to implement the Observer Pattern.
 *
 * @author Nathan Wagenknecht, David Yang, Nick Weinschrott, Noah Villanueva, Andrew Thomas
 * @version 1.0
 * @created 10-Oct-2019 9:26:51 AM
 */
public interface Observer {

    /**
     * Pull data from the subject.
     *
     * @param gtfsData The GTFSData instance.
     */
    void pull(GTFSData gtfsData);
}
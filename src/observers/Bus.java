/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package observers;

import subject.GTFSData;

/**
 * Represents the Bus concrete observer and its properties and relationships.
 *
 * @author Nathan Wagenknecht, David Yang, Nick Weinschrott, Noah Villanueva, Andrew Thomas
 * @version 1.0
 * @created 10-Oct-2019 9:26:55 AM
 */
public class Bus implements Observer {

    private int busLat;
    private int busLon;
    private GTFSData mGTFSData;

    /**
     * Constructor for a Bus object.
     *
     * @param busLat The initial latitude of the bus.
     * @param busLon The initial longitude of the bus.
     */
    public Bus(int busLat, int busLon) {
        mGTFSData = GTFSData.getInstance();

        if (!mGTFSData.getObservers().contains(this)) {
            mGTFSData.addObserver(this);
        }

        //requestUpdate();

        this.busLat = busLat;
        this.busLon = busLon;
    }

    /**
     * Returns a String representation of the Bus.
     *
     * @return a String representing a Bus Object
     */
    @Override
    public String toString() {
        return "\n\n[Bus #_ _ _ Lat: " + busLat + " Lon: " + busLon;
    }

    /**
     * Returns the observer's status.
     *
     * @return String representing status
     */
    public String status() {
        return null;
    }

    //Getters

    public int getBusLat() {
        return busLat;
    }

    public void setBusLat(int busLat) {
        this.busLat = busLat;
    }

    //Setters

    public int getBusLon() {
        return busLon;
    }

    public void setBusLon(int busLon) {
        this.busLon = busLon;
    }

    /**
     * Observer Pattern implementation.
     *
     * @param gtfsData The GTFSData instance.
     */
    @Override
    public void pull(GTFSData gtfsData) {
    }
}
/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package observers;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.LatLongBounds;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapShape;
import com.lynden.gmapsfx.javascript.object.MapTypeIdEnum;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;
import com.lynden.gmapsfx.service.directions.DirectionStatus;
import com.lynden.gmapsfx.service.directions.DirectionsRenderer;
import com.lynden.gmapsfx.service.directions.DirectionsResult;
import com.lynden.gmapsfx.service.geocoding.GeocodingService;
import dataManagement.Route;
import dataManagement.Stop;
import dataManagement.StopTime;
import dataManagement.Trip;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.util.Callback;
import subject.GTFSData;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * The embodiment of a GTFS Google Map Observer.
 *
 * @author Nathan Wagenknecht
 */
public class GTFSGoogleMapObserver implements Observer {

    private GTFSData mGTFSData;
    private MapOptions mMapOptions;
    private GoogleMapView mGoogleMapView;
    private GoogleMap mGoogleMap;

    private ListView<Route> mMapRouteListView;
    private ListView<Trip> mMapTripListView;
    private ListView<Stop> mMapStopListView;

    private TextField mStopLabelTextField;
    private TextField mMapStatDispayText;
    private ImageView mRouteIconImageView;
    private ImageView mTripIconImageView;

    private DirectionsRenderer mDirectionsRenderer;
    //list of markers on the map
    private List<Marker> mRouteMarkers;
    private List<Marker> mTripMarkers;
    private Marker mStopHighlightMarker;
    //list of shapes on the map
    private List<MapShape> mTripPolylines;

    private Route mSelectedRoute;
    private Trip mSelectedTrip;
    private Stop mSelectedStop;

    private static final double INITIAL_LATITUDE = 43.044457;
    private static final double INITIAL_LONGITUDE = -87.907428;
    private static final int INITIAL_ZOOM = 18;

    /**
     * Constructor for GTFSGoogleMapObserver.
     *
     * @param googleMapView The GoogleMapView.
     * @param mapRouteListView The ListView for Route.
     * @param mapTripListView The ListView for Trip.
     * @param mapStopListView The ListView for Stop.
     * @param stopLabelTextField The TextField for the Stop Label.
     * @param mapStatDisplayText The TextField for the Map Stat Display Text.
     * @param routeIconImageView The ImageView for the Route Icon.
     * @param tripIconImageView The ImageView for the Trip Icon.
     */
    public GTFSGoogleMapObserver(GoogleMapView googleMapView,
                                 ListView<Route> mapRouteListView,
                                 ListView<Trip> mapTripListView,
                                 ListView<Stop> mapStopListView,
                                 TextField stopLabelTextField,
                                 TextField mapStatDisplayText,
                                 ImageView routeIconImageView,
                                 ImageView tripIconImageView) {

        this.mGTFSData = GTFSData.getInstance();

        if (!mGTFSData.getObservers().contains(this)) {
            mGTFSData.addObserver(this);
        }

        this.mStopLabelTextField = stopLabelTextField;
        this.mMapStatDispayText = mapStatDisplayText;
        this.mRouteIconImageView = routeIconImageView;
        this.mTripIconImageView = tripIconImageView;

        //lists to hold polylines and markers that are on the map
        this.mRouteMarkers = new ArrayList<>();
        this.mTripMarkers = new ArrayList<>();
        this.mTripPolylines = new ArrayList<>();

        //the google map view that holds the map object
        this.mGoogleMapView = googleMapView;
        mGoogleMapView.setKey("AIzaSyCxSCtqOnJwXHyV0_QvzT8hL9x8-Z3hXl0");

        //GUI list objects that display routes and trips to the user
        this.mMapRouteListView = mapRouteListView;
        this.mMapTripListView = mapTripListView;
        this.mMapStopListView = mapStopListView;

        //the map direction renderer
        this.mDirectionsRenderer = null;

        initializeEventListeners();
        initializeListViews();

    }

    /**
     * Implements the Observer Pattern.
     *
     * @param gtfsData The GTFSData instance.
     */
    @Override
    public void pull(GTFSData gtfsData) {
        this.mGTFSData = gtfsData;
        //always keep the route list updated and in sync with currently loaded files
        ObservableList<Route> routeList = FXCollections.observableArrayList();
        routeList.addAll(gtfsData.getRoutes());
        //sort the displayed routes alphabetically
        mMapRouteListView.setItems(routeList);
        mMapRouteListView.getItems().sort(Comparator.comparing(Route::toString));
    }

    //Initializers

    /**
     * Sets all of the List views on the dynamic map tab to single selection mode.
     */
    private void initializeListViews() {
        mMapRouteListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        mMapTripListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        mMapStopListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
    }

    private void initializeEventListeners() {
        //RouteListView listeners
        mMapRouteListView.setOnKeyPressed((keyEvent) -> {
            if ((keyEvent.getCode() == KeyCode.DOWN ||
                    keyEvent.getCode() == KeyCode.UP ||
                    keyEvent.getCode() == KeyCode.ENTER) &&
                    mMapRouteListView.getSelectionModel().getSelectedItem() != null) {
                handleRouteSelect();
            }
        });

        mMapRouteListView.setOnMouseClicked((mouseEvent) -> {
            if (mouseEvent.getButton() == MouseButton.PRIMARY &&
                    mMapRouteListView.getSelectionModel().getSelectedItem() != null) {
                handleRouteSelect();
            }
        });

        //TripListView listeners
        mMapTripListView.setOnKeyPressed((keyEvent) -> {
            if ((keyEvent.getCode() == KeyCode.DOWN ||
                    keyEvent.getCode() == KeyCode.UP ||
                    keyEvent.getCode() == KeyCode.ENTER) &&
                    mMapTripListView.getSelectionModel().getSelectedItem() != null) {
                handleTripSelect();
            }
        });

        mMapTripListView.setOnMouseClicked((mouseEvent) -> {
            if (mouseEvent.getButton() == MouseButton.PRIMARY &&
                    mMapTripListView.getSelectionModel().getSelectedItem() != null) {
                handleTripSelect();
            }
        });

        //StopListView listeners
        mMapStopListView.setOnKeyPressed((keyEvent) -> {
            if ((keyEvent.getCode() == KeyCode.DOWN ||
                    keyEvent.getCode() == KeyCode.UP ||
                    keyEvent.getCode() == KeyCode.ENTER) &&
                    mMapStopListView.getSelectionModel().getSelectedItem() != null) {
                handleStopSelect();
            }
        });

        mMapStopListView.setOnMouseClicked((mouseEvent) -> {
            if (mouseEvent.getButton() == MouseButton.PRIMARY &&
                    mMapStopListView.getSelectionModel().getSelectedItem() != null) {
                handleStopSelect();
            }
        });
    }

    /**
     * Initializes the Map.
     */
    public void initializeMap() {
        mMapOptions = new MapOptions()
                .center(new LatLong(INITIAL_LATITUDE, INITIAL_LONGITUDE))
                .mapType(MapTypeIdEnum.ROADMAP)
                .mapTypeControl(true)
                .rotateControl(false)
                .streetViewControl(false)
                .zoom(INITIAL_ZOOM);

        mGoogleMap = mGoogleMapView.createMap(mMapOptions);
        mDirectionsRenderer =
                new DirectionsRenderer(false, mGoogleMapView.getMap(), mGoogleMapView.getDirec());
    }

    private void handleRouteSelect() {
        mSelectedRoute = mMapRouteListView.getSelectionModel().getSelectedItem();

        mRouteIconImageView.setImage(new Image(getRouteColorURL(mSelectedRoute.getRouteColor())));
        mTripIconImageView.setImage(new Image(getRouteColorURL(mSelectedRoute.getRouteColor())));

        clearRouteMarkers();
        clearTripMarkers();
        clearTripPolyines();
        clearStopHighlightMarker();
        mMapTripListView.getItems().clear();
        mMapTripListView.getSelectionModel().clearSelection();
        mMapStopListView.getItems().clear();
        mMapStopListView.getSelectionModel().clearSelection();

        List<Stop> stops = getStopsAssosiatedWithRoute(mSelectedRoute);
        if (!stops.isEmpty()) {
            mMapStatDispayText.setText(stops.size() + " stops on selected route");
            centerMapOnStops(stops);
            drawRouteMarkers(stops);

            //populate trip listview with associated trips
            ObservableList<Trip> tripObservableList = FXCollections.observableArrayList();
            tripObservableList.addAll(getTripsAssociatedWithRoute(mSelectedRoute));
            mMapTripListView.setItems(tripObservableList);
            mMapTripListView.getItems().sort(Comparator.comparing(Trip::toString));

            //populated stop listview with associated stops
            ObservableList<Stop> stopObservableList = FXCollections.observableArrayList();
            stopObservableList.addAll(stops);

            mMapStopListView.setCellFactory(new Callback<ListView<Stop>, ListCell<Stop>>() {
                @Override
                public ListCell<Stop> call(ListView<Stop> stopListView) {
                    ListCell<Stop> cell = new ListCell<>() {
                        @Override
                        protected void updateItem(Stop item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item != null) {
                                setText(item.toString());
                            } else {
                                setText(null);
                            }
                        }
                    };
                    return cell;
                }
            });

            mMapStopListView.setItems(stopObservableList);
            mMapStopListView.getItems().sort(Comparator.comparing(Stop::toString));
        } else {
            mMapStatDispayText.setText("no stops on selected route");
        }

        mStopLabelTextField.setText("Stops (Route)");
    }

    private void handleTripSelect() {
        mSelectedTrip = mMapTripListView.getSelectionModel().getSelectedItem();

        clearRouteMarkers();
        clearTripMarkers();
        clearTripPolyines();
        clearStopHighlightMarker();
        mMapStopListView.getItems().clear();
        mMapStopListView.getSelectionModel().clearSelection();

        List<Stop> stops = getStopsAssociatedWithTrip(mSelectedTrip);
        if (!stops.isEmpty()) {
            mMapStatDispayText.setText(stops.size() + " unique stops on selected trip");
            centerMapOnStops(stops);
            drawTripMarkers(stops);
            //drawTripPolyLine(stops);

            //populated stop listview with associated stops
            ObservableList<Stop> stopObservableList = FXCollections.observableArrayList();
            stopObservableList.addAll(stops);

            mMapStopListView.setCellFactory(new Callback<ListView<Stop>, ListCell<Stop>>() {
                @Override
                public ListCell<Stop> call(ListView<Stop> stopListView) {
                    ListCell<Stop> cell = new ListCell<>() {
                        @Override
                        protected void updateItem(Stop item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item != null) {
                                setText((mMapStopListView.getItems().indexOf(item) + 1)
                                        + " : " + item.toString());
                            } else {
                                setText(null);
                            }
                        }
                    };
                    return cell;
                }
            });

            mMapStopListView.setItems(stopObservableList);
        }
        mStopLabelTextField.setText("Stops (Trip)");

    }

    private void handleStopSelect() {
        mSelectedStop = mMapStopListView.getSelectionModel().getSelectedItem();
        clearStopHighlightMarker();
        drawStopHighlightMarker(mSelectedStop);
        mGoogleMap.panTo(new LatLong(mSelectedStop.getStopLat(), mSelectedStop.getStopLon()));
    }

    //map transform functions
    private void centerMapOnStops(List<Stop> stops) {
        LatLongBounds bounds = new LatLongBounds(findSWLatLong(stops), findNELatLong(stops));
        mGoogleMap.fitBounds(bounds);
        //mGoogleMap.setZoom(13);
        //mGoogleMap.panTo(latLong);
    }

    private LatLong findSWLatLong(List<Stop> stops) {
        double lowestLat = stops.get(0).getStopLat();
        double lowestLon = stops.get(0).getStopLon();

        for (Stop stop : stops) {
            if (stop.getStopLat() < lowestLat) {
                lowestLat = stop.getStopLat();
            }
            if (stop.getStopLon() < lowestLon) {
                lowestLon = stop.getStopLon();
            }
        }
        return new LatLong(lowestLat, lowestLon);
    }

    private LatLong findNELatLong(List<Stop> stops) {
        double largestLat = stops.get(0).getStopLat();
        double largestLon = stops.get(0).getStopLon();

        for (Stop stop : stops) {
            if (stop.getStopLat() > largestLat) {
                largestLat = stop.getStopLat();
            }
            if (stop.getStopLon() > largestLon) {
                largestLon = stop.getStopLon();
            }
        }
        return new LatLong(largestLat, largestLon);
    }

    //map helper methods
    /**
     * finds all stops associated with given route
     *
     * @param route route to find stops associated with
     * @return list of stops associated with given route, null if none were found
     */
    private List<Stop> getStopsAssosiatedWithRoute(Route route) {
        List<Stop> stopsAssociatedWithRoute = new ArrayList<>();

        List<Trip> tripsAssociatedWithRoute = getTripsAssociatedWithRoute(route);
        for (Trip trip : tripsAssociatedWithRoute) {
            List<Stop> stopsAssociatedWithTrip = getStopsAssociatedWithTrip(trip);
            if (stopsAssociatedWithTrip != null) {
                for (Stop stop : stopsAssociatedWithTrip) {
                    if (stop != null) {
                        if (!stopsAssociatedWithRoute.contains(stop)) {
                            stopsAssociatedWithRoute.add(stop);
                        }
                    }
                }
            }
        }
        return stopsAssociatedWithRoute;
    }

    /**
     * finds all trips associated with given route
     *
     * @param route route to find trips associated with
     * @return list of trips associated with given route, null if none were found
     * @throws NoSuchElementException in case of a missing element
     */
    private List<Trip> getTripsAssociatedWithRoute(Route route) throws NoSuchElementException {
        List<Trip> associatedTrips = new ArrayList<>();

        for (Trip trip : mGTFSData.getTrips()) {
            if (trip.getRouteId().equals(route.getRouteId())) {
                if (!associatedTrips.contains(trip)) {
                    associatedTrips.add(trip);
                }
            }
        }
        return associatedTrips;
    }

    /**
     * Gets all of the stops on a trip sorted by the trip sequence
     * (last stop is the same as the first)
     *
     * @param trip trip to get associated stops from
     * @return list of associated stops in order of the trip stop sequence, null if none were found
     * @throws NoSuchElementException in case of a missing element
     */
    private List<Stop> getStopsAssociatedWithTrip(Trip trip) throws NoSuchElementException {
        List<Stop> associatedStops = new ArrayList<>();

        //facilitates sorting of StopTime by stopSequence
        ObservableList<StopTime> stopTimeObservableList =
                FXCollections.observableArrayList(trip.getStopTimes());
        for (StopTime stopTime : stopTimeObservableList) {
            Stop associatedStop = mGTFSData.getStop(stopTime.getStopId());
            if (associatedStop != null) {
                associatedStops.add(associatedStop);
            }
        }
        return associatedStops;
    }

    private String getRouteColorURL(String color) {
        switch (color) {
            case "005BAB":
                return "http://labs.google.com/ridefinder/images/mm_20_blue.png";
            case "00ADEF":
                return "http://labs.google.com/ridefinder/images/mm_20_blue.png";
            case "6D2383":
                return "http://labs.google.com/ridefinder/images/mm_20_purple.png";
            case "FFDD00":
                return "http://labs.google.com/ridefinder/images/mm_20_yellow.png";
            case "F5821F":
                return "http://labs.google.com/ridefinder/images/mm_20_orange.png";
            case "FFFFFF":
                return "http://labs.google.com/ridefinder/images/mm_20_white.png";
            case "008345":
                return "http://labs.google.com/ridefinder/images/mm_20_green.png";
            case "D61D24":
                return "http://labs.google.com/ridefinder/images/mm_20_red.png";
            case "61BB46":
                return "http://labs.google.com/ridefinder/images/mm_20_green.png";
            case "000000":
                return "http://labs.google.com/ridefinder/images/mm_20_black.png";
            case "74392D":
                return "http://labs.google.com/ridefinder/images/mm_20_brown.png";
            default:
                return "http://labs.google.com/ridefinder/images/mm_20_red.png";


        }
    }

    //map drawing functions
    private void drawRouteMarkers(List<Stop> stops) {
        for (Stop stop : stops) {
            if (stop != null) {
                MarkerOptions markerOptions = new MarkerOptions()
                        //.icon("http://maps.google.com/mapfiles/ms/micons/bus.png")
                        .icon(getRouteColorURL(mSelectedRoute.getRouteColor()))
                        .position(new LatLong(stop.getStopLat(), stop.getStopLon()));
                mGoogleMap.addMarker(createRouteMarker(markerOptions));
            }
        }
        refreshMapView();
    }

    private void drawTripMarkers(List<Stop> stops) {
        if (stops.get(0).equals(stops.get(stops.size() - 1))) {
            MarkerOptions firstMarkerOptions = new MarkerOptions()
                    .icon("http://maps.google.com/mapfiles/kml/paddle/red-square.png")
                    .position(new LatLong(stops.get(0).getStopLat(), stops.get(0).getStopLon()));
            mGoogleMap.addMarker(createTripMarker(firstMarkerOptions));
        } else {
            MarkerOptions firstMarkerOptions = new MarkerOptions()
                    .icon("http://maps.google.com/mapfiles/kml/paddle/A.png")
                    .position(new LatLong(stops.get(0).getStopLat(), stops.get(0).getStopLon()));
            mGoogleMap.addMarker(createTripMarker(firstMarkerOptions));

            MarkerOptions lastMarkerOptions = new MarkerOptions()
                    .icon("http://maps.google.com/mapfiles/kml/paddle/B.png")
                    .position(new LatLong(stops.get(stops.size() - 1).getStopLat(),
                            stops.get(stops.size() - 1).getStopLon()));
            mGoogleMap.addMarker(createTripMarker(lastMarkerOptions));
            refreshMapView();
        }

        for (int i = 0; i < stops.size() - 1; i++) {
            MarkerOptions middleMarkerOptions = new MarkerOptions()
                    .icon(getRouteColorURL(mSelectedRoute.getRouteColor()))
                    .position(new LatLong(stops.get(i).getStopLat(), stops.get(i).getStopLon()));
            Marker marker = createTripMarker(middleMarkerOptions);
            mGoogleMap.addMarker(marker);
        }


    }

    private void drawStopHighlightMarker(Stop stop) {
        MarkerOptions markerOptions = new MarkerOptions()
                .icon("http://maps.google.com/mapfiles/ms/micons/yellow.png")
                .position(new LatLong(stop.getStopLat(), stop.getStopLon()));
        Marker marker = new Marker(markerOptions);
        mStopHighlightMarker = marker;
        mGoogleMap.addMarker(marker);
        refreshMapView();
    }

    private Marker createRouteMarker(MarkerOptions markerOptions) {
        Marker marker = new Marker(markerOptions);
        mRouteMarkers.add(marker);
        return marker;
    }

    private Marker createTripMarker(MarkerOptions markerOptions) {
        Marker marker = new Marker(markerOptions);
        markerOptions.title("test");
        markerOptions.visible(true);
        mTripMarkers.add(marker);
        return marker;
    }

    private void clearRouteMarkers() {
        if (!mRouteMarkers.isEmpty()) {
            mGoogleMap.removeMarkers(mRouteMarkers);
            mRouteMarkers.clear();
        }
        clearStopHighlightMarker();
    }

    private void clearTripMarkers() {
        if (!mTripMarkers.isEmpty()) {
            mGoogleMap.removeMarkers(mTripMarkers);
            mTripMarkers.clear();
        }
        clearStopHighlightMarker();
    }

    private void clearStopHighlightMarker() {
        if (mStopHighlightMarker != null) {
            mGoogleMap.removeMarker(mStopHighlightMarker);
            mStopHighlightMarker = null;
        }
    }

    private void clearTripPolyines() {
        if (!mTripPolylines.isEmpty()) {
            for (MapShape mapShape : mTripPolylines) {
                mGoogleMap.removeMapShape(mapShape);
            }
            mTripPolylines.clear();
        }
    }

    private void refreshMapView() {
        int currentZoom = mGoogleMap.getZoom();
        mGoogleMap.setZoom(currentZoom - 1);
        mGoogleMap.setZoom(currentZoom);
    }

    /**
     * Direction Debugger Helper.
     * Formerly had @Override.
     *
     * @param directionsResult The directions result to pass in.
     * @param directionStatus The direction status to pass in.
     */
    public void directionsReceivedDebug(DirectionsResult directionsResult,
                                        DirectionStatus directionStatus) {
        if (directionStatus.equals(DirectionStatus.OK)) {
            System.out.println("OK");

            DirectionsResult e = directionsResult;
            GeocodingService gs = new GeocodingService();

            System.out.println("SIZE ROUTES: " + e.getRoutes().size() + "\n" + "ORIGIN: "
                    + e.getRoutes().get(0).getLegs().get(0).getStartLocation());
            System.out.println("LEGS SIZE: " + e.getRoutes().get(0).getLegs().size());
            System.out.println("WAYPOINTS " + e.getGeocodedWaypoints().size());
            try {
                System.out.println("Distancia total = " +
                        e.getRoutes().get(0).getLegs().get(0).getDistance().getText());
            } catch (Exception ex) {
                System.out.println("ERRO: " + ex.getMessage());
            }
            System.out.println("LEG(0)");
            System.out.println(e.getRoutes().get(0).getLegs().get(0).getSteps().size());
            System.out.println(mDirectionsRenderer.toString());
        }
    }
}

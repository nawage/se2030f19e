/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package dataManagement;

import java.time.LocalTime;

/**
 * A GTFS-friendly representation of the time.
 */
public class GTFSTime {
    private int hour;
    private int minute;
    private int second;
    private static final int ZERO_PLACEHOLDER_THRESHOLD = 10;

    /**
     * Constructor for a GTFSTime object.
     *
     * @param time the pre-formatted time as a String
     */
    public GTFSTime(String time) {
        String[] elements = time.split(":");
        this.hour = Integer.parseInt(elements[0]);
        this.minute = Integer.parseInt(elements[1]);
        this.second = Integer.parseInt(elements[2]);
    }

    /**
     * Constructor from a LocalTime object
     *
     * @param time the LocalTime object
     */
    public GTFSTime(LocalTime time) {
        this.hour = time.getHour();
        this.minute = time.getMinute();
        this.second = time.getSecond();
    }

    /**
     * Constructor with set time values.
     *
     * @param hour int value representing hour
     * @param minute int value representing minute
     * @param second int value representing second
     */
    public GTFSTime(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    //Getters and Setters
    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    /**
     * Compare to method used for comparing GTFSTime objects to each other.
     * Used for making a sorted arrayList.
     *
     * @param gtfsTime the GTFSTime variable being compared
     * @return A 0 if the times are equal, a -1 if the argument is later,
     * and a 1 if this time is later than the argument.
     */
    public int compareTo(GTFSTime gtfsTime) {
        if (gtfsTime.getHour() > hour) {
            return -1;
        } else if (hour > gtfsTime.getHour()){
            return 1;
        } else if (gtfsTime.getMinute() > minute) {
            return -1;
        } else if (minute > gtfsTime.getMinute()) {
            return 1;
        } else if (gtfsTime.getSecond() > second) {
            return -1;
        } else if (second > gtfsTime.getSecond()) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Returns a String representation of GTFSTime.
     *
     * @return a String representation of GTFSTime.
     */
    @Override
    public String toString() {
        return convertLongToString(hour) + ":" +
                convertLongToString(minute) + ":" + convertLongToString(second);
    }

    /**
     * Converts a long to a String.
     *
     * @param time The long to convert.
     * @return The String form of the long passed in.
     */
    private String convertLongToString(long time) {
        if (time < ZERO_PLACEHOLDER_THRESHOLD) {
            return "0" + time;
        } else {
            return "" + time;
        }
    }
}

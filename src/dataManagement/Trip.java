/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package dataManagement;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.validators.MustMatchRegexExpression;
import com.opencsv.bean.validators.PreAssignmentValidator;
import subject.GTFSData;

import java.util.LinkedList;

/**
 * Represents a Transit Trip and its properties and relationships.
 *
 * @author David Yang
 * @author Andrew Thomas
 * @author Nathan Wagenknecht - Annotations
 * @version 1.0
 * @created 10-Oct-2019 9:10:36 AM
 */
public class Trip implements GTFSDataType {
    private LinkedList<StopTime> stopTimes;
    private static final int SECONDS_PER_HOUR = 3600;
    private static final int MINUTES_PER_HOUR = 60;
    private static final double EARTH_RADIUS_MILES = 3958.8;

    /**
     * routes.route.id
     * Identifies a route
     */
    @CsvBindByName(column = "route_id", required = true)
    private String routeId;

    /**
     * calendar.serviceId
     * calendarDates.serviceId
     * Identifies a set of dates when service is available for one or more routes
     */
    @CsvBindByName(column = "service_id", required = true)
    private String serviceId;

    /**
     * Identifies trip
     */
    @CsvBindByName(column = "trip_id", required = true)
    private String tripId;

    @CsvBindByName(column = "trip_headsign")
    private String tripHeadSign;

    /**
     * Indicates the direction of travel for a trip
     * 0 - Travel in one direction (outbound)
     * 1- Travel in the opposite direction (inbound)
     */
    @PreAssignmentValidator(validator = MustMatchRegexExpression.class, paramString = "($^|[0-1])")
    @CsvBindByName(column = "direction_id")
    private String directionId;

    /**
     * Identifies the block to which the trip belongs
     */
    @CsvBindByName(column = "block_id")
    private String blockId;

    /**
     * shape.shape,id
     * Identifies a geospatial shape describing the vehicle travel path for a trip
     */
    @CsvBindByName(column = "shape_id")
    private String shapeId;


    /**
     * Default Constructor
     */
    public Trip() {
        //do not remove, this is needed for the CSV import functionality
        this.stopTimes = new LinkedList<>();
    }

    /**
     * Constructor of Trip Class
     *
     * @param routeId      - Id of a route
     * @param serviceId    - Dates when service is available
     * @param tripId       - Id of a trip
     * @param tripHeadSign - sign displaying a trip
     * @param directionId  - Direction of travel
     * @param blockId      - block in which trip belongs
     * @param shapeId      - Geospatial shape
     */
    public Trip(String routeId, String serviceId, String tripId, String tripHeadSign,
                String directionId, String blockId, String shapeId) {
        this();
        this.blockId = blockId;
        this.directionId = directionId;
        this.routeId = routeId;
        this.serviceId = serviceId;
        this.shapeId = shapeId;
        this.tripId = tripId;
        this.tripHeadSign = tripHeadSign;
    }

    /**
     * Return original raw data.
     *
     * @return - raw data
     */
    public String toString() {
        return tripId;
    }


    /**
     * Adds a stop time to an LinkedList containing stop times.
     *
     * @param stopTime - time a trip stops
     */
    public void addStopTime(StopTime stopTime) {
        if (this.stopTimes.isEmpty()) {
            this.stopTimes.add(stopTime);
        } else {
            int position = 0;
            for (StopTime stopTimeInList: stopTimes) {
                int comparison = stopTime.getStopSequence() - stopTimeInList.getStopSequence();
                if (comparison == 0) {
                    position++;
                    break;
                } else if (comparison <= -1) {
                    break;
                } else {
                    position++;
                }
            }
            if (position >= stopTimes.size()) {
                stopTimes.add(stopTime);
            } else {
                stopTimes.add(position, stopTime);
            }
        }
    }

    /**
     * Returns the average speed in miles per hour.
     *
     * @return The average speed in miles per hour.
     */
    public double getAverageSpeed() {
        double seconds = getTotalTime();
        double hours = seconds / (double)SECONDS_PER_HOUR;
        double miles = getTotalDistance();
        if(hours == 0.0) {
            return 0;
        }
        return miles / hours;
    }

    /**
     * Calculate Total Distance.
     * Formula Credit/Source:
     * https://stackoverflow.com/questions/27928 (URL continued on next line)
     * /calculate-distance-between-two-latitude-longitude-points-haversine-formula
     * https://stackoverflow.com/questions/1502590/
     * calculate-distance-between-two-points-in-google-maps-v3
     *
     * @return The total distance.
     */
    public double getTotalDistance() {
        double totalDistance = 0.0;
        for(int i = 0; i <= stopTimes.size()-2; i++){
            Stop stop1 = GTFSData.getInstance().getStop(stopTimes.get(i).getStopId());
            double lat1 = Math.toRadians(stop1.getStopLat());
            double lon1 = Math.toRadians(stop1.getStopLon());
            Stop stop2 = GTFSData.getInstance().getStop(stopTimes.get(i+1).getStopId());
            double lat2 = Math.toRadians(stop2.getStopLat());
            double lon2 = Math.toRadians(stop2.getStopLon());
            double lat21 = lat2 - lat1;
            double lon21 = lon2 - lon1;
            double step1 = Math.pow(Math.sin(lat21/2.0), 2) +
                           Math.cos(lat1) * Math.cos(lat2) *
                           Math.pow(Math.sin(lon21/2.0), 2);
            double step2 = 2 * Math.atan2(Math.sqrt(step1), Math.sqrt(1 - step1));
            double step3 = EARTH_RADIUS_MILES * step2;
            totalDistance += step3;
        }
        return totalDistance;
    }

    /**
     * Gets the total time between and within stopTimes.
     *
     * @return The total time between and within stopTimes.
     */
    private double getTotalTime() {
        return getTotalTimesBetweenStopTimes() + getTotalTimesWithinStopTimes();
    }

    /**
     * Calculates the total time between stopTimes.
     *
     * @return the total time between stopTimes.
     */
    private double getTotalTimesBetweenStopTimes() {
        double totalTime = 0;
        for (int i = 0; i <= stopTimes.size() - 2; i++) {
            totalTime += getTimeBetweenStopTimes(stopTimes.get(i), stopTimes.get(i + 1));
        }
        return totalTime;
    }

    /**
     * Calculates the time between departure-arrival of two stopTimes.
     *
     * @param departingStopTime The time of departure from the first stopTime.
     * @param arrivingStopTime The time of arrival at the second stopTime.
     * @return The time between departure-arrival of two stopTimes.
     */
    private double getTimeBetweenStopTimes(StopTime departingStopTime, StopTime arrivingStopTime) {
        if (departingStopTime.getDepartureTime() == arrivingStopTime.getArrivalTime()) {
            return 0;
        }
        GTFSTime departureTime = departingStopTime.getDepartureTime();
        GTFSTime arrivalTime = arrivingStopTime.getArrivalTime();

        long departureSeconds = departureTime.getSecond();
        long departureMinutes = departureTime.getMinute() * MINUTES_PER_HOUR;
        long departureHours = departureTime.getHour() * SECONDS_PER_HOUR;
        long departureTimeInSeconds = departureHours + departureMinutes + departureSeconds;

        long arrivalSeconds = arrivalTime.getSecond();
        long arrivalMinutes = arrivalTime.getMinute() * MINUTES_PER_HOUR;
        long arrivalHours = arrivalTime.getHour() * SECONDS_PER_HOUR;
        long arrivalTimeInSeconds = arrivalHours + arrivalMinutes + arrivalSeconds;

        return arrivalTimeInSeconds - departureTimeInSeconds;
    }

    /**
     * Returns the total time within all stops in seconds.
     *
     * @return the total time within all stops in seconds.
     */
    private double getTotalTimesWithinStopTimes() {
        long totalTime = 0;
        if (stopTimes.size() <= 0) {
            return totalTime;
        }
        for (StopTime stopTime : stopTimes) {
            totalTime += stopTime.getDurationAtStop();
        }
        return totalTime;
    }

    /**
     * Determines if the given Trip is 'similar' to the Trip calling this method.
     * 'Similar' means the trips have a common start Stop or common end Stop (or both).
     *
     * @param trip that is passed in to compare
     * @return Whether or not the Trips are similar.
     */
    public boolean isSimilar(Trip trip) {
        return false;
    }

    //Getters and Setters
    public String getBlockId() {
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }

    public String getDirectionId() {
        return directionId;
    }

    public void setDirectionId(String directionId) {
        this.directionId = directionId;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getShapeId() {
        return shapeId;
    }

    public void setShapeId(String shapeId) {
        this.shapeId = shapeId;
    }

    public String getTripHeadSign() {
        return tripHeadSign;
    }

    public void setTripHeadSign(String tripHeadSign) {
        this.tripHeadSign = tripHeadSign;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public LinkedList<StopTime> getStopTimes() {
        return stopTimes;
    }
}
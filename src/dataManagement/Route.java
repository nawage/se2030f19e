package dataManagement;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.processor.PreAssignmentProcessor;
import com.opencsv.bean.validators.PreAssignmentValidator;
import fileManagement.processors.DefaultValueStringProcessor;
import fileManagement.validators.ColorValidator;

import java.io.Serializable;

/**
 * Represents a Transit Route and its properties and relationships.
 *
 * @author Andrew Thomas
 * @Author Nathan Wagenknecht - Annotations
 * @version 1.0
 * @created 10-Oct-2019 9:26:50 AM
 */
public class Route implements GTFSDataType {

    @CsvBindByName(column = "route_id", required = true)
    private String routeId;

    @CsvBindByName(column = "agency_id")
    private String agencyId;

    @CsvBindByName(column = "route_short_name")
    private String routeShortName;

    @CsvBindByName(column = "route_long_name")
    private String routeLongName;

    @CsvBindByName(column = "route_desc")
    private String routeDescription;

    @CsvBindByName(column = "route_type", required = true)
    private int routeType;

    @CsvBindByName(column = "route_url")
    private String routeUrl;

    @PreAssignmentProcessor(processor = DefaultValueStringProcessor.class, paramString = "000000")
    @PreAssignmentValidator(validator = ColorValidator.class)
    @CsvBindByName(column = "route_color", required = true)
    private String routeColor;

    @PreAssignmentProcessor(processor = DefaultValueStringProcessor.class, paramString = "000000")
    @PreAssignmentValidator(validator = ColorValidator.class)
    @CsvBindByName(column = "route_text_color")
    private String routeTextColor;

    /**
     * Default Constructor
     */
    public Route() {
        //do not remove, this is needed for the CSV import functionality
    }

    /**
     * Constructor
     *
     * @param routeId String value representing ID
     * @param agencyId String representing the Agency
     * @param routeShortName String value representing the short route name
     * @param routeLongName String representation of the Route's long na,e
     * @param routeDescription String description of the route
     * @param routeType int value representing the route type
     * @param routeUrl String representing URL
     * @param routeColor String representing Color
     * @param routeTextColor String representing text color
     */
    public Route(String routeId, String agencyId, String routeShortName,
                 String routeLongName, String routeDescription, int routeType,
                 String routeUrl, String routeColor, String routeTextColor) {
        this.agencyId = agencyId;
        this.routeId = routeId;
        this.routeShortName = routeShortName;
        this.routeLongName = routeLongName;
        this.routeDescription = routeDescription;
        this.routeType = routeType;
        this.routeUrl = routeUrl;
        this.routeColor = routeColor;
        this.routeTextColor = routeTextColor;
    }

    /**
     * Returns a String representation of Route
     *
     * @return a String representation of Route
     */
    @Override
    public String toString() {
        return routeLongName + " : " + routeId;
    }

    //Getters

    public String getAgencyId() {
        return agencyId;
    }

    public String getRouteId() {
        return routeId;
    }

    public String getRouteShortName() {
        return routeShortName;
    }

    public String getRouteLongName() {
        return routeLongName;
    }

    public String getRouteDescription() {
        return routeDescription;
    }

    public int getRouteType() {
        return routeType;
    }

    public String getRouteUrl() {
        return routeUrl;
    }

    public String getRouteColor() {
        return routeColor;
    }

    public String getRouteTextColor() {
        return routeTextColor;
    }

    //Setters

    public void setAgencyId(String agencyId) {
        this.agencyId = agencyId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public void setRouteShortName(String routeShortName) {
        this.routeShortName = routeShortName;
    }

    public void setRouteLongName(String routeLongName) {
        this.routeLongName = routeLongName;
    }

    public void setRouteDescription(String routeDescription) {
        this.routeDescription = routeDescription;
    }

    public void setRouteType(int routeType) {
        this.routeType = routeType;
    }

    public void setRouteUrl(String routeUrl) {
        this.routeUrl = routeUrl;
    }

    public void setRouteColor(String routeColor) {
        this.routeColor = routeColor;
    }

    public void setRouteTextColor(String routeTextColor) {
        this.routeTextColor = routeTextColor;
    }
}
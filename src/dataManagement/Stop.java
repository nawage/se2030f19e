/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package dataManagement;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.validators.PreAssignmentValidator;
import fileManagement.validators.LatituteValidator;
import fileManagement.validators.LongitudeValidator;

import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

/**
 * Represents a Transit Stop and its properties and relationships.
 *
 * @author Andrew Thomas
 * @author Nathan Wagenknecht - Annotations
 * @version 1.0
 * @created 10-Oct-2019 9:10:34 AM
 */
public class Stop implements GTFSDataType {

    @CsvBindByName(column = "stop_id", required = true)
    private String stopId;
    @CsvBindByName(column = "stop_name")
    private String stopName;
    @CsvBindByName(column = "stop_desc")
    private String stopDescription;
    @PreAssignmentValidator(validator = LatituteValidator.class)
    @CsvBindByName(column = "stop_lat")
    private double stopLat;
    @PreAssignmentValidator(validator = LongitudeValidator.class)
    @CsvBindByName(column = "stop_lon")
    private double stopLon;

    private LinkedList<StopTime> stopTimes;

    /**
     * Default Constructor for Stop Object.
     */
    public Stop() {
        //do not remove, this is needed for the CSV import functionality
        this.stopTimes = new LinkedList<>();
    }

    /**
     * Constructor for Stop Object.
     *
     * @param stopId The Stop ID.
     * @param stopName The Stop Name.
     * @param stopDescription The Stop Description.
     * @param stopLat The Stop Latitude.
     * @param stopLon The Stop Longitude.
     */
    public Stop(String stopId, String stopName,
                String stopDescription, double stopLat, double stopLon) {
        this.stopId = stopId;
        this.stopName = stopName;
        this.stopDescription = stopDescription;
        this.stopLat = stopLat;
        this.stopLon = stopLon;
        this.stopTimes = new LinkedList<>();
    }



    /**
     * Adds a StopTime object to the LinkedList of stopTime objects.
     * The method is designed to sort the StopTimes by their departureTimes.
     * The list is sorted from StopTimes with the earliest departureTime
     * to StopTime objects with the latest departureTime.
     *
     * @param stopTime the stopTime object being added to the List
     */
    public void addStopTime(StopTime stopTime) {
        if (this.stopTimes.size() <= 0) {
            stopTimes.add(stopTime);
        } else {
            int position = 0;
            for (StopTime stopTimeInList: stopTimes) {
                int comparison =
                        stopTime.getDepartureTime().compareTo(stopTimeInList.getDepartureTime());
                if (comparison == 0) {
                    position++;
                    break;
                } else if (comparison == -1) {
                    break;
                } else {
                    position++;
                }
            }
            if (position >= stopTimes.size()) {
                stopTimes.add(stopTime);
            } else {
                stopTimes.add(position, stopTime);
            }
        }
    }

    /**
     * Return the data in its raw format
     *
     * @return - raw data
     */
    @Override
    public String toString() {
        return stopName + " : " + stopId;
    }

    //Getters

    /**
     * Returns the next Trip departing from this stop
     *
     * @return the StopTime that contains the id of the next Trip,
     * defaults to the first StopTime if current time is
     * later than any of the StopTimes, and returns
     * null if there are no StopTimes in the list.
     */
    public StopTime getNextTrip() {
        if (stopTimes.size() == 0) {
            return null;
        } else {
            LocalTime tempCurrentTime = LocalTime.now();
            GTFSTime currentTime = new GTFSTime(tempCurrentTime);
            for (StopTime stopTime : stopTimes) {
                if (currentTime.compareTo(stopTime.getDepartureTime()) < 0) {
                    return stopTime;
                }
            }
            return stopTimes.get(0);
        }
    }

    /**
     * Returns the next Trip departing from this stop.
     * This method is used for testing and ensuring the logic works.
     * Takes a specific time instead of creating a LocalTime using the (.now) method.
     *
     * @author Nick Weinschrott
     * @param localTime represents the current time and the nest trip will start after this time
     * @return the StopTime with the next departureTime or the first stopTime
     * in the list if the current time is later than any times in the list.
     */
    public StopTime getNextTrip(LocalTime localTime) {
        if (stopTimes.size() == 0) {
            return null;
        } else {
            GTFSTime currentTime = new GTFSTime(localTime);
            for (StopTime stopTime : stopTimes) {
                if (currentTime.compareTo(stopTime.getDepartureTime()) < 0) {
                    return stopTime;
                }
            }
            return stopTimes.get(0);
        }
    }

    /**
     * Returns a List of Strings containing the IDs of every trip that goes through the stop.
     *
     * @return a LinkedList containing all the Trip IDs
     */
    public LinkedList<String> getTripsOnStop(){
        LinkedList<String> tripIds = new LinkedList<>();
        for (StopTime stopTime: stopTimes) {
            String trip = stopTime.getTripId();
            if (!tripIds.contains(trip)) {
                tripIds.add(trip);
            }
        }
        return tripIds;
    }


    public Route getRoutesContainingStop(){
        return null;
    }
    public int getNumTrips() {
        return 0;
    }

    public List<Route> getRoutesThroughStop() {
        return null;
    }

    public List<Trip> getTrips() {
        return null;
    }

    public String getStopId() {
        return stopId;
    }

    public String getStopName() {
        return stopName;
    }

    public String getStopDescription() {
        return stopDescription;
    }

    public double getStopLat() {
        return stopLat;
    }

    public double getStopLon() {
        return stopLon;
    }

    //Setters

    public void setStopId(String stopId) {
        this.stopId = stopId;
    }

    public void setStopName(String stopName) {
        this.stopName = stopName;
    }

    public void setStopDescription(String stopDescription) {
        this.stopDescription = stopDescription;
    }

    public void setStopLat(int stopLat) {
        this.stopLat = stopLat;
    }

    public void setStopLon(int stopLon) {
        this.stopLon = stopLon;
    }

}
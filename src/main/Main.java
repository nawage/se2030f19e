package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main Class to run JavaFX program.
 *
 * @author weinschrottn
 * @version 1.0
 * @created 10-Oct-2019 9:10:26 AM
 */
public class Main extends Application {
    private static final int MID_HEIGHT = 764;
    private static final int MID_WIDTH = 1078;

    /**
     * Initializes GUI
     *
     * @param stage - the main stage of the program
     * @throws Exception Something goes wrong within the program.
     */
    @Override
    public void start(Stage stage) throws Exception {
        // Load FXML file
        Parent root = FXMLLoader.load(getClass().getResource("../fxmlFiles/main_menu.fxml"));

        // Initialize stage properties
        stage.setScene(new Scene(root));
        stage.setTitle("GTFS Tool");
        stage.setMinHeight(MID_HEIGHT);
        stage.setMinWidth(MID_WIDTH);
        stage.show();
        System.out.println("Min height = " + stage.getMinHeight());
        System.out.println("Min Width = " + stage.getMinWidth());

    }

    /**
     * Main Method. Launches the JavaFX Program.
     *
     * @param args command-line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
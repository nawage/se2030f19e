package main;

/**
 * Contains all possible System Message formats to be displayed to the user.
 *
 * @author weinschrottn
 * @version 1.0
 * @created 10-Oct-2019 9:26:56 AM
 */
public class SystemMessages {

    /**
     * Displays a System Message (informative).
     *
     * @param systemMessage The message to be displayed.
     */
    public static void displayInfoMessage(String systemMessage) {

    }

    /**
     * Displays an Error Message.
     *
     * @param errorMessage The message to be displayed.
     */
    public static void displayErrorMessage(String errorMessage) {

    }

    /**
     * Displays a Confirmation Message (takes user input).
     *
     * @param confirmationMessage The message to be displayed.
     */
    public static void displayConfirmationDialog(String confirmationMessage) {

    }

}
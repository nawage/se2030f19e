package subject;

import observers.Observer;

/**
 * The Subject Interface necessary to implement the Observer Pattern.
 *
 * @author Nathan Wagenknecht, David Yang, Nick Weinschrott, Noah Villanueva, Andrew Thomas
 * @version 1.0
 * @created 10-Oct-2019 9:10:33 AM
 */
public interface Subject {

    /**
     * Add the given observer to the list of observers.
     *
     * @param o The particular Observer to add.
     */
    void addObserver(Observer o);

    /**
     * Delete the given observer from the list of observers.
     *
     * @param o The particular Observer to delete.
     */
    void deleteObserver(Observer o);

    /**
     * Go through the internal list of observers and notify of changes.
     */
    void notifyObservers();

}
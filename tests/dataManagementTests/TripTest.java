package dataManagementTests;

import dataManagement.Stop;
import dataManagement.StopTime;
import dataManagement.Trip;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

class TripTest {
    private Trip trip;

    /**
     * Creates a new Trip before every test
     */
    @BeforeEach
    void setUp() {
        this.trip = new Trip(null, null, null, null, null, null, null);
    }

    /**
     * Tests the Trip object getAverageSpeed() method if all the StopTimes take place in one day
     */
    @Test
    public void testGetAverageSpeed() {
        //Tests if there have not been any Stops Added
        assertEquals(0.0, trip.getAverageSpeed());

        //Tests if there has only been One Stop
        Stop stop1 = new Stop("1", "one", "desc", 42.9451148, -88.0042294);
        StopTime stopTime1 = new StopTime(null, "01:30:00", "00:00:00", "1", 0, null, 0, 0);
        trip.addStopTime(stopTime1);
        assertEquals(0, trip.getAverageSpeed());

        //Tests if there have been two stops added
        Stop stop2 = new Stop("2", "two", "desc", 42.9446866, -88.0037764);
        StopTime stopTime2 = new StopTime(null, "20:30:00", "20:00:00", "2"  , 0, null, 0, 0);
        trip.addStopTime(stopTime2);
        // assertNotEquals(0, trip.getAverageSpeed());
    }

    /**
     * tests that add stops are added in a sorted order
     */
    @Test
    public void testAddStopTime(){
        StopTime stopTime1 = new StopTime(null, "00:00:00", "00:00:00", null, 0, null, 0, 0);
        StopTime stopTime2 = new StopTime(null, "00:00:00", "00:00:00", null, 1, null, 0, 0);
        StopTime stopTime3 = new StopTime(null, "00:00:00", "00:00:00", null, 2, null, 0, 0);
        StopTime stopTime4 = new StopTime(null, "00:00:00", "00:00:00", null, 3, null, 0, 0);
        StopTime stopTime5 = new StopTime(null, "00:00:00", "00:00:00", null, 4, null, 0, 0);


        //Tests that the method adds a StopTime if the list is empty
        trip.addStopTime(stopTime1);
        LinkedList<StopTime> testList1 = new LinkedList<>();
        testList1.add(stopTime1);
        assertEquals(testList1, trip.getStopTimes());

        //Tests that the method correctly adds a later StopTime to the list
        trip.addStopTime(stopTime5);
        LinkedList<StopTime> testList2 = new LinkedList<>();
        testList2.add(stopTime1);
        testList2.add(stopTime5);
        assertEquals(testList2, trip.getStopTimes());

        /*Tests that the method correctly adds a StopTime to it's position in the list if it's sequence number is less
        `than the total size of the List
         */
        trip.addStopTime(stopTime2);
        LinkedList<StopTime> testList3 = new LinkedList<>();
        testList3.add(stopTime1);
        testList3.add(stopTime2);
        testList3.add(stopTime5);
        assertEquals(testList3, trip.getStopTimes());

        /*Tests that the method will correctly add the stopTime to the list at it's position
         */
        trip.addStopTime(stopTime4);
        LinkedList<StopTime> testList4 = new LinkedList<>();
        testList4.add(stopTime1);
        testList4.add(stopTime2);
        testList4.add(stopTime4);
        testList4.add(stopTime5);
        assertEquals(testList4, trip.getStopTimes());

        /*Tests that the method will correctly add the stopTime to the list at it's position
         */
        trip.addStopTime(stopTime3);
        LinkedList<StopTime> testList5 = new LinkedList<>();
        testList5.add(stopTime1);
        testList5.add(stopTime2);
        testList5.add(stopTime3);
        testList5.add(stopTime4);
        testList5.add(stopTime5);
        assertEquals(testList5, trip.getStopTimes());
    }
}
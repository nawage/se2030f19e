package dataManagementTests;

import dataManagement.GTFSTime;
import dataManagement.StopTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for StopTime class's constructor and toString.
 *
 * @author David Yang
 **/
class StopTimeTest {
    private StopTime stopTime;
    private GTFSTime departureGTFSTime;
    private GTFSTime arrivalGTFSTime;

    /**
     * Sets up new instance variables before every test
     */
    @BeforeEach
    void setUp() {
        departureGTFSTime = new GTFSTime("00:00:00");
        arrivalGTFSTime = new GTFSTime("00:00:00");
    this.stopTime = new StopTime("", "00:00:00", "00:00:00", "", 0, "", 0, 0);

    }

    /**
     * tests getDurationMethod
     */
    @Test
    public void testDurationGoingIntoNewDay(){
        departureGTFSTime = new GTFSTime("00:01:00");
        arrivalGTFSTime = new GTFSTime("23:58:00");
        stopTime.setArrivalTime(arrivalGTFSTime);
        stopTime.setDepartureTime(departureGTFSTime);
        assertEquals(180, stopTime.getDurationAtStop());
    }
}
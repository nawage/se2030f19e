package fileManagementTests;

import com.opencsv.exceptions.CsvException;
import dataManagement.Route;
import dataManagement.Stop;
import dataManagement.StopTime;
import dataManagement.Trip;
import fileManagement.FileHandler;
import fileManagement.GTFSCSVParser;
import fileManagement.verifiers.RouteVerifier;
import fileManagement.verifiers.StopTimeVerifier;
import fileManagement.verifiers.StopVerifier;
import fileManagement.verifiers.TripVerifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

 /**
 * Tests the GTFSCSVParser with a varied set of valid and invalid files
 * goal is to test cases where header values and line values are incorrect for the appropriate GTFS file format
 *
 * @author Nathan Wagenknecht
 **/
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
class GTFSCSVParserTest {
    private GTFSCSVParser<Route> routeGTFSCSVParser;
    private GTFSCSVParser<Stop> stopGTFSCSVParser;
    private GTFSCSVParser<StopTime> stopTimeGTFSCSVParser;
    private GTFSCSVParser<Trip> tripGTFSCSVParser;

    //sets up a GTFSCSVParser for each file type
    @BeforeEach
    void setUp() {
        routeGTFSCSVParser = new GTFSCSVParser<>(Route.class, new RouteVerifier<>(), FileHandler.ROUTES_HEADER_LINE);
        stopGTFSCSVParser = new GTFSCSVParser<>(Stop.class, new StopVerifier<>(), FileHandler.STOPS_HEADER_LINE);
        stopTimeGTFSCSVParser = new GTFSCSVParser<>(StopTime.class, new StopTimeVerifier<>(), FileHandler.TRIPS_HEADER_LINE);
        tripGTFSCSVParser = new GTFSCSVParser<>(Trip.class, new TripVerifier<>(), FileHandler.STOP_TIMES_HEADER_LINE);
    }

    //Route Tests
    @Test
    public void TestValidGenericRoutesFile() {
        parseRoutesFile("tests/GTFS_ValidationFiles/routes/valid/routes.txt");

        assertEquals(0, routeGTFSCSVParser.getNumWarnings());
        assertEquals(0, routeGTFSCSVParser.getNumErrors());
        assertEquals(10, routeGTFSCSVParser.getNumVerifiedLines());
        //expects file to be considered valid after parsing
        assertTrue(routeGTFSCSVParser.isFileValid());

        System.out.println(routeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidChicagoRoutesFile() {
        parseRoutesFile("tests/Complete_GTFS_Files/GTFS_Chicago/routes.txt");

        assertEquals(0, routeGTFSCSVParser.getNumWarnings());
        assertEquals(0, routeGTFSCSVParser.getNumErrors());
        assertEquals(135, routeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(routeGTFSCSVParser.isFileValid());

        System.out.println(routeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidEauClaireRoutesFile() {
        parseRoutesFile("tests/Complete_GTFS_Files/GTFS_EauClaire/routes.txt");

        assertEquals(0, routeGTFSCSVParser.getNumWarnings());
        assertEquals(0, routeGTFSCSVParser.getNumErrors());
        assertEquals(21, routeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(routeGTFSCSVParser.isFileValid());

        System.out.println(routeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidLAXRoutesFile() {
        parseRoutesFile("tests/Complete_GTFS_Files/GTFS_LAX/routes.txt");

        assertEquals(0, routeGTFSCSVParser.getNumWarnings());
        assertEquals(0, routeGTFSCSVParser.getNumErrors());
        assertEquals(10, routeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(routeGTFSCSVParser.isFileValid());

        System.out.println(routeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidMCTSRoutesFile() {
        parseRoutesFile("tests/Complete_GTFS_Files/GTFS_MCTS/routes.txt");

        assertEquals(0, routeGTFSCSVParser.getNumWarnings());
        assertEquals(0, routeGTFSCSVParser.getNumErrors());
        assertEquals(62, routeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(routeGTFSCSVParser.isFileValid());

        System.out.println(routeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestRoutesFileWithExtraFields() {
        parseRoutesFile("tests/GTFS_ValidationFiles/routes/extraFields/routes.txt");

        assertEquals(0, routeGTFSCSVParser.getNumWarnings());
        assertEquals(1, routeGTFSCSVParser.getNumErrors());
        assertEquals(1, routeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(9, routeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(2, routeGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(routeGTFSCSVParser.isFileValid());

        System.out.println(routeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestRoutesFileWithInvalidColor() {
        parseRoutesFile("tests/GTFS_ValidationFiles/routes/InvalidColor/routes.txt");

        assertEquals(0, routeGTFSCSVParser.getNumWarnings());
        assertEquals(1, routeGTFSCSVParser.getNumErrors());
        assertEquals(1, routeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(9, routeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(2, routeGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(routeGTFSCSVParser.isFileValid());

        System.out.println(routeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestRoutesFileWithMissingData() {
        parseRoutesFile("tests/GTFS_ValidationFiles/routes/missingData/routes.txt");

        assertEquals(0, routeGTFSCSVParser.getNumWarnings());
        assertEquals(1, routeGTFSCSVParser.getNumErrors());
        assertEquals(1, routeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(9, routeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(2, routeGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(routeGTFSCSVParser.isFileValid());

        System.out.println(routeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestRoutesFileWithMissingData2() {
        parseRoutesFile("tests/GTFS_ValidationFiles/routes/MissingData2/routes.txt");

        assertEquals(0, routeGTFSCSVParser.getNumWarnings());
        assertEquals(1, routeGTFSCSVParser.getNumErrors());
        assertEquals(1, routeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(9, routeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(2, routeGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(routeGTFSCSVParser.isFileValid());

        System.out.println(routeGTFSCSVParser.getParserReport());
    }

    //Stop time tests

    @Test
    public void TestValidGenericStopTimesFile() {
        parseStopTimesFile("tests/GTFS_ValidationFiles/stop_times/valid/stop_times.txt");

        assertEquals(0, stopTimeGTFSCSVParser.getNumWarnings());
        assertEquals(0, stopTimeGTFSCSVParser.getNumErrors());
        assertEquals(0, stopTimeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(31309, stopTimeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(stopTimeGTFSCSVParser.isFileValid());

        System.out.println(stopTimeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidChicagoStopTimesFile() {
        parseStopTimesFile("tests/Complete_GTFS_Files/GTFS_Chicago/stop_times.txt");

        assertEquals(0, stopTimeGTFSCSVParser.getNumWarnings());
        assertEquals(0, stopTimeGTFSCSVParser.getNumErrors());
        assertEquals(0, stopTimeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(2809049, stopTimeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(stopTimeGTFSCSVParser.isFileValid());

        System.out.println(stopTimeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidChicagoStopTimesFileWithOverflow() {
        parseStopTimesFile("tests/GTFS_ValidationFiles/stop_times/lotsOfErrors/stop_times.txt");

        assertEquals(0, stopTimeGTFSCSVParser.getNumWarnings());
        assertEquals(299815, stopTimeGTFSCSVParser.getNumErrors());
        assertEquals(299815, stopTimeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(3760667, stopTimeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(stopTimeGTFSCSVParser.isFileValid());

        System.out.println(stopTimeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidEauClaireStopTimesFile() {
        parseStopTimesFile("tests/Complete_GTFS_Files/GTFS_EauClaire/stop_times.txt");

        assertEquals(0, stopTimeGTFSCSVParser.getNumWarnings());
        assertEquals(0, stopTimeGTFSCSVParser.getNumErrors());
        assertEquals(0, stopTimeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(14715, stopTimeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(stopTimeGTFSCSVParser.isFileValid());

        System.out.println(stopTimeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidLAXStopTimesFile() {
        parseStopTimesFile("tests/Complete_GTFS_Files/GTFS_LAX/stop_times.txt");

        assertEquals(0, stopTimeGTFSCSVParser.getNumWarnings());
        assertEquals(0, stopTimeGTFSCSVParser.getNumErrors());
        assertEquals(0, stopTimeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(27313, stopTimeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(stopTimeGTFSCSVParser.isFileValid());

        System.out.println(stopTimeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidMCTSStopTimesFile() {
        parseStopTimesFile("tests/Complete_GTFS_Files/GTFS_MCTS/stop_times.txt");

        assertEquals(0, stopTimeGTFSCSVParser.getNumWarnings());
        assertEquals(0, stopTimeGTFSCSVParser.getNumErrors());
        assertEquals(0, stopTimeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(659753, stopTimeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(stopTimeGTFSCSVParser.isFileValid());

        System.out.println(stopTimeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestStopTimesFileWithExtraField() {
        parseStopTimesFile("tests/GTFS_ValidationFiles/stop_times/extraField/stop_times.txt");

        assertEquals(0, stopTimeGTFSCSVParser.getNumWarnings());
        assertEquals(1, stopTimeGTFSCSVParser.getNumErrors());
        assertEquals(1, stopTimeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(31308, stopTimeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(2, stopTimeGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(stopTimeGTFSCSVParser.isFileValid());

        System.out.println(stopTimeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestStopTimesFileWithInvalidTimeFormat() {
        parseStopTimesFile("tests/GTFS_ValidationFiles/stop_times/InvalidTimeFormat/stop_times.txt");

        assertEquals(0, stopTimeGTFSCSVParser.getNumWarnings());
        assertEquals(1, stopTimeGTFSCSVParser.getNumErrors());
        assertEquals(1, stopTimeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(31308, stopTimeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(2, stopTimeGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(stopTimeGTFSCSVParser.isFileValid());

        System.out.println(stopTimeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestStopTimesFileWithMissingArrivalTime() {
        parseStopTimesFile("tests/GTFS_ValidationFiles/stop_times/missingArrivalTime/stop_times.txt");

        assertEquals(0, stopTimeGTFSCSVParser.getNumWarnings());
        assertEquals(1, stopTimeGTFSCSVParser.getNumErrors());
        assertEquals(1, stopTimeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(31308, stopTimeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(2, stopTimeGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(stopTimeGTFSCSVParser.isFileValid());

        System.out.println(stopTimeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestStopTimesFileWithMissingDepartureTime() {
        parseStopTimesFile("tests/GTFS_ValidationFiles/stop_times/missingDepartureTime/stop_times.txt");

        assertEquals(0, stopTimeGTFSCSVParser.getNumWarnings());
        assertEquals(1, stopTimeGTFSCSVParser.getNumErrors());
        assertEquals(1, stopTimeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(31308, stopTimeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(2, stopTimeGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(stopTimeGTFSCSVParser.isFileValid());

        System.out.println(stopTimeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestStopTimesFileWithMissingField() {
        parseStopTimesFile("tests/GTFS_ValidationFiles/stop_times/missingField/stop_times.txt");

        assertEquals(0, stopTimeGTFSCSVParser.getNumWarnings());
        assertEquals(1, stopTimeGTFSCSVParser.getNumErrors());
        assertEquals(1, stopTimeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(31308, stopTimeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(3, stopTimeGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(stopTimeGTFSCSVParser.isFileValid());

        System.out.println(stopTimeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestStopTimesFileWithMissingStopID() {
        parseStopTimesFile("tests/GTFS_ValidationFiles/stop_times/missingStopID/stop_times.txt");

        assertEquals(0, stopTimeGTFSCSVParser.getNumWarnings());
        assertEquals(1, stopTimeGTFSCSVParser.getNumErrors());
        assertEquals(1, stopTimeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(31308, stopTimeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(4, stopTimeGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(stopTimeGTFSCSVParser.isFileValid());

        System.out.println(stopTimeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestStopTimesFileWithMissingTripID() {
        parseStopTimesFile("tests/GTFS_ValidationFiles/stop_times/missingTripID/stop_times.txt");

        assertEquals(0, stopTimeGTFSCSVParser.getNumWarnings());
        assertEquals(1, stopTimeGTFSCSVParser.getNumErrors());
        assertEquals(1, stopTimeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(31308, stopTimeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(2, stopTimeGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(stopTimeGTFSCSVParser.isFileValid());

        System.out.println(stopTimeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestStopTimesFileWithNegativeSequence() {
        parseStopTimesFile("tests/GTFS_ValidationFiles/stop_times/negativeSequence/stop_times.txt");

        assertEquals(0, stopTimeGTFSCSVParser.getNumWarnings());
        assertEquals(1, stopTimeGTFSCSVParser.getNumErrors());
        assertEquals(1, stopTimeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(31308, stopTimeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(2, stopTimeGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(stopTimeGTFSCSVParser.isFileValid());

        System.out.println(stopTimeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestStopTimesFileWithNonIntSequence() {
        parseStopTimesFile("tests/GTFS_ValidationFiles/stop_times/nonIntSequence/stop_times.txt");

        assertEquals(0, stopTimeGTFSCSVParser.getNumWarnings());
        assertEquals(1, stopTimeGTFSCSVParser.getNumErrors());
        assertEquals(1, stopTimeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(31308, stopTimeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(2, stopTimeGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(stopTimeGTFSCSVParser.isFileValid());

        System.out.println(stopTimeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestStopTimesFileWithTimeOutOfRange() {
        parseStopTimesFile("tests/GTFS_ValidationFiles/stop_times/timeOutOfRange/stop_times.txt");

        assertEquals(0, stopTimeGTFSCSVParser.getNumWarnings());
        assertEquals(1, stopTimeGTFSCSVParser.getNumErrors());
        assertEquals(1, stopTimeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(31308, stopTimeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(2, stopTimeGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(stopTimeGTFSCSVParser.isFileValid());

        System.out.println(stopTimeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidGenericStopsFile() {
        parseStopsFile("tests/GTFS_ValidationFiles/stops/validEC/stops.txt");

        assertEquals(0, stopGTFSCSVParser.getNumWarnings());
        assertEquals(0, stopGTFSCSVParser.getNumErrors());
        assertEquals(0, stopGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(510, stopGTFSCSVParser.getNumVerifiedLines());
        assertTrue(stopGTFSCSVParser.isFileValid());

        System.out.println(stopGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidChicagoStopsFile() {
        parseStopsFile("tests/Complete_GTFS_Files/GTFS_Chicago/stops.txt");

        assertEquals(0, stopGTFSCSVParser.getNumWarnings());
        assertEquals(0, stopGTFSCSVParser.getNumErrors());
        assertEquals(0, stopGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(11395, stopGTFSCSVParser.getNumVerifiedLines());
        assertTrue(stopGTFSCSVParser.isFileValid());

        System.out.println(stopGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidEauClaireStopsFile() {
        parseStopsFile("tests/Complete_GTFS_Files/GTFS_EauClaire/stops.txt");

        assertEquals(0, stopGTFSCSVParser.getNumWarnings());
        assertEquals(0, stopGTFSCSVParser.getNumErrors());
        assertEquals(0, stopGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(510, stopGTFSCSVParser.getNumVerifiedLines());
        assertTrue(stopGTFSCSVParser.isFileValid());

        System.out.println(stopGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidLAXStopsFile() {
        parseStopsFile("tests/Complete_GTFS_Files/GTFS_LAX/stops.txt");

        assertEquals(0, stopGTFSCSVParser.getNumWarnings());
        assertEquals(0, stopGTFSCSVParser.getNumErrors());
        assertEquals(0, stopGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(433, stopGTFSCSVParser.getNumVerifiedLines());
        assertTrue(stopGTFSCSVParser.isFileValid());

        System.out.println(stopGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidMCTSStopsFile() {
        parseStopsFile("tests/Complete_GTFS_Files/GTFS_MCTS/stops.txt");

        assertEquals(0, stopGTFSCSVParser.getNumWarnings());
        assertEquals(0, stopGTFSCSVParser.getNumErrors());
        assertEquals(0, stopGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(5392, stopGTFSCSVParser.getNumVerifiedLines());
        assertTrue(stopGTFSCSVParser.isFileValid());

        System.out.println(stopGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestStopsFileWithExtraData() {
        parseStopsFile("tests/GTFS_ValidationFiles/stops/invalidECExtraData/stops.txt");

        assertEquals(0, stopGTFSCSVParser.getNumWarnings());
        assertEquals(1, stopGTFSCSVParser.getNumErrors());
        assertEquals(1, stopGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(509, stopGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(2, stopGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(stopGTFSCSVParser.isFileValid());

        System.out.println(stopGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestStopsFileWithGPSOutOfRange() {
        parseStopsFile("tests/GTFS_ValidationFiles/stops/invalidECGPSOutOfRange/stops.txt");

        assertEquals(0, stopGTFSCSVParser.getNumWarnings());
        assertEquals(1, stopGTFSCSVParser.getNumErrors());
        assertEquals(1, stopGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(509, stopGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(2, stopGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(stopGTFSCSVParser.isFileValid());

        System.out.println(stopGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestStopsFileWithMissingData() {
        parseStopsFile("tests/GTFS_ValidationFiles/stops/invalidECMissingData/stops.txt");

        assertEquals(0, stopGTFSCSVParser.getNumWarnings());
        assertEquals(1, stopGTFSCSVParser.getNumErrors());
        assertEquals(1, stopGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(509, stopGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(2, stopGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(stopGTFSCSVParser.isFileValid());

        System.out.println(stopGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestStopsFileWithMissingField() {
        parseStopsFile("tests/GTFS_ValidationFiles/stops/invalidECMissingField/stops.txt");

        assertEquals(0, stopGTFSCSVParser.getNumWarnings());
        assertEquals(1, stopGTFSCSVParser.getNumErrors());
        assertEquals(1, stopGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(509, stopGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(2, stopGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(stopGTFSCSVParser.isFileValid());

        System.out.println(stopGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidGenericTripsFile() {
        parseTripsFile("tests/GTFS_ValidationFiles/trips/valid/trips.txt");

        assertEquals(0, tripGTFSCSVParser.getNumWarnings());
        assertEquals(0, tripGTFSCSVParser.getNumErrors());
        assertEquals(0, tripGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(639, tripGTFSCSVParser.getNumVerifiedLines());
        assertTrue(tripGTFSCSVParser.isFileValid());

        System.out.println(tripGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidChicagoTripsFile() {
        parseTripsFile("tests/Complete_GTFS_Files/GTFS_Chicago/trips.txt");

        assertEquals(0, tripGTFSCSVParser.getNumWarnings());
        assertEquals(0, tripGTFSCSVParser.getNumErrors());
        assertEquals(0, tripGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(47331, tripGTFSCSVParser.getNumVerifiedLines());
        assertTrue(tripGTFSCSVParser.isFileValid());

        System.out.println(tripGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidEauClaireTripsFile() {
        parseTripsFile("tests/Complete_GTFS_Files/GTFS_EauClaire/trips.txt");

        assertEquals(0, tripGTFSCSVParser.getNumWarnings());
        assertEquals(0, tripGTFSCSVParser.getNumErrors());
        assertEquals(0, tripGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(361, tripGTFSCSVParser.getNumVerifiedLines());
        assertTrue(tripGTFSCSVParser.isFileValid());

        System.out.println(tripGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidLAXTripsFile() {
        parseTripsFile("tests/Complete_GTFS_Files/GTFS_LAX/trips.txt");

        assertEquals(0, tripGTFSCSVParser.getNumWarnings());
        assertEquals(0, tripGTFSCSVParser.getNumErrors());
        assertEquals(0, tripGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(639, tripGTFSCSVParser.getNumVerifiedLines());
        assertTrue(tripGTFSCSVParser.isFileValid());

        System.out.println(tripGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestValidMCTSripsFile() {
        parseTripsFile("tests/Complete_GTFS_Files/GTFS_MCTS/trips.txt");

        assertEquals(0, tripGTFSCSVParser.getNumWarnings());
        assertEquals(0, tripGTFSCSVParser.getNumErrors());
        assertEquals(0, tripGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(9300, tripGTFSCSVParser.getNumVerifiedLines());
        assertTrue(tripGTFSCSVParser.isFileValid());

        System.out.println(tripGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestTripsFileWithExtraFields() {
        parseTripsFile("tests/GTFS_ValidationFiles/trips/extraFields/trips.txt");

        assertEquals(0, tripGTFSCSVParser.getNumWarnings());
        assertEquals(1, tripGTFSCSVParser.getNumErrors());
        assertEquals(1, tripGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(638, tripGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(3, tripGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(tripGTFSCSVParser.isFileValid());

        System.out.println(tripGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestTripsFileWithMissingData() {
        parseTripsFile("tests/GTFS_ValidationFiles/trips/missingData/trips.txt");

        assertEquals(0, tripGTFSCSVParser.getNumWarnings());
        assertEquals(1, tripGTFSCSVParser.getNumErrors());
        assertEquals(1, tripGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(638, tripGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(2, tripGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(tripGTFSCSVParser.isFileValid());

        System.out.println(tripGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestTripsFileWithMissingData2() {
        parseTripsFile("tests/GTFS_ValidationFiles/trips/missingData2/trips.txt");

        assertEquals(0, tripGTFSCSVParser.getNumWarnings());
        assertEquals(1, tripGTFSCSVParser.getNumErrors());
        assertEquals(1, tripGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(638, tripGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumber(3, tripGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(tripGTFSCSVParser.isFileValid());

        System.out.println(tripGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestStopTimesFileWithMultipleErrorsAndWarnings() {
        parseStopTimesFile("tests/GTFS_ValidationFiles/stop_times/multipleErrorsAndWarnings/stop_times.txt");

        assertEquals(3, stopTimeGTFSCSVParser.getNumWarnings());
        assertEquals(8, stopTimeGTFSCSVParser.getNumErrors());
        assertEquals(8, stopTimeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(31301, stopTimeGTFSCSVParser.getNumVerifiedLines());
        assertTrue(containsLineNumbers(new int[]{13, 19, 55, 69, 76, 87, 117, 127}, stopTimeGTFSCSVParser.getCapturedCSVExceptions()));
        assertTrue(stopTimeGTFSCSVParser.isFileValid());

        System.out.println(stopTimeGTFSCSVParser.getParserReport());
    }

    @Test
    public void TestStopTimesFileWithInvalidHeader() {
        parseStopTimesFile("tests/GTFS_ValidationFiles/stop_times/InvalidHeader/stop_times.txt");

        assertEquals(4, stopTimeGTFSCSVParser.getNumWarnings());
        assertEquals(1, stopTimeGTFSCSVParser.getNumErrors());
        assertEquals(0, stopTimeGTFSCSVParser.getNumCapturedExceptions());
        assertEquals(0, stopTimeGTFSCSVParser.getNumVerifiedLines());
        assertFalse(stopTimeGTFSCSVParser.isFileValid());

        System.out.println(stopTimeGTFSCSVParser.getParserReport());
    }



    //Testing helper methods
    //file parser helper methods reduce redundant IO exception catching code

    //parses a routes file
    private void parseRoutesFile(String filename) {
        try {
            routeGTFSCSVParser.parseCSVFile(new File(filename));
        } catch(FileNotFoundException e){
            e.printStackTrace();
        }
    }

    //parses a stops file
    private void parseStopsFile(String filename) {
        try {
            stopGTFSCSVParser.parseCSVFile(new File(filename));
        } catch(FileNotFoundException e){
            e.printStackTrace();
        }
    }

    //parses a stop times file
    private void parseStopTimesFile(String filename) {
        try {
            stopTimeGTFSCSVParser.parseCSVFile(new File(filename));
        } catch(FileNotFoundException e){
            e.printStackTrace();
        }
    }

    //parses a trips file
    private void parseTripsFile(String filename) {
        try {
            tripGTFSCSVParser.parseCSVFile(new File(filename));
        } catch(FileNotFoundException e){
            e.printStackTrace();
        }
    }

    //line number  helper methods reduce redundant looping code

    /**
     * checks if a given line number is found in a list of CsvExceptions
     *
     * @param lineNumber line number to check
     * @param csvExceptionList list of CsVExceptions to compare against
     * @return true if the line number is found in the list of CsVExceptions, false if it is not
     */
    private boolean containsLineNumber(int lineNumber, List<CsvException> csvExceptionList) {
        for (CsvException e : csvExceptionList) {
            if (lineNumber == e.getLineNumber()) {
                return true;
            }
        }
        return false;
    }

    /**
     * checks if a set of line numbers are all found in a list of CsVExceptions
     *
     * @param lineNumbers line numbers to check
     * @param csvExceptionList list of CsVExceptions to compare against
     * @return false if any line number is not found in the list of CsVExceptions, true otherwise
     */
    private boolean containsLineNumbers(int[] lineNumbers, List<CsvException> csvExceptionList) {
        for (int lineNumber : lineNumbers) {
            if (!containsLineNumber(lineNumber, csvExceptionList)) {
                return false;
            }
        }
        return true;
    }
}